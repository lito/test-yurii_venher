-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 12, 2015 at 02:26 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tp`
--

-- --------------------------------------------------------

--
-- Table structure for table `Album`
--

CREATE TABLE IF NOT EXISTS `Album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `identifiedWith` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Album`
--

INSERT INTO `Album` (`id`, `title`, `intro`, `body`, `identifiedWith`, `link`) VALUES
(1, 'Ultimate Casual Collection', 'Nec ex harum homero. No repudiare maiestatis est. Ei odio meliore intellegebat eam, ad inani simul vim. Ne iuvaret disputationi his. Pri ex brute propriae assueverit, pro eu veri viderer. No mollis theophrastus qui, duo lorem ullum nemore ea, ne vel solet', '', 'works', 'http://tatjana-prijmak.eu/'),
(2, 'SOMETHING for the WEEKEND', 'Duo eu epicurei definitionem, nam unum semper id. Et similique disputationi mel. An usu illum affert neglegentur, erant luptatum reprehendunt his in. Facer dolor aliquando sea et, persius docendi ea vis. Odio delenit eloquentiam ei quo. Eum fugit perpetua', '', 'works', 'http://tatjana-prijmak.eu/');

-- --------------------------------------------------------

--
-- Table structure for table `Article`
--

CREATE TABLE IF NOT EXISTS `Article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `categoryId` int(11) NOT NULL,
  `documents` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `AuthContent`
--

CREATE TABLE IF NOT EXISTS `AuthContent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `relatedToUser` int(11) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `AuthContent`
--

INSERT INTO `AuthContent` (`id`, `token`, `relatedToUser`, `isAdmin`) VALUES
(9, '172eee54aa664e9dd0536b063796e54e', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `BaseContent`
--

CREATE TABLE IF NOT EXISTS `BaseContent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `documentId` int(11) NOT NULL,
  `identifiedWith` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `lastModified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `BaseContent`
--

INSERT INTO `BaseContent` (`id`, `documentId`, `identifiedWith`, `link`, `title`, `body`, `lastModified`) VALUES
(1, 0, 'about', '#', 'About', '<h3>Who We Are</h3><h2>L<span style="font-size: 14px; line-height: 1.42857143; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">orem ipsum dolor sit amet, nec viris quaerendum ex, mei no sale ferri constituto. Cu vim consulatu argumentum referrentur, est cu nulla voluptatum. Te apeirian consequat nec. Pro assum munere consequuntur an. Eu est vulputate temporibus accommodare, veniam civibus honestatis quo cu, mel ut soluta eirmod quaestio.&nbsp;</span><span style="font-size: 14px; line-height: 1.42857143; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">Pri probatus recteque salutatus ne, mei ne novum ubique, pri iuvaret efficiantur comprehensam ex.</span></h2><div class="image-container static" data-static="true" data-static-type="image" style="max-width: 1920px; max-height: 711px;"><div class="image" style="padding-bottom: 37.03125%; height: 0px;"><img src="/uploads/generic/01.jpg" style="max-width: 100%;"></div></div><div><h2><span style="line-height: 1.42857143;">I</span><span style="line-height: 1.42857143; font-size: 14px; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">usto officiis voluptaria ad eos, nam id cibo simul consectetuer. Mnesarchum sadipscing scribentur eu vel, mea id soluta dicunt. Ex duo harum eligendi, has cu recteque voluptaria. Ius in voluptatum reprimique, quis sint oporteat et vim, liber melius iudicabit ius ei.</span></h2><div class="image-container static" data-static="true" data-static-type="image" style="max-width: 1920px; max-height: 711px;"><div class="image" style="padding-bottom: 37.03125%; height: 0px;"><img src="/uploads/generic/04.jpg" style="max-width: 100%;"></div></div><div><h2><span style="line-height: 1.42857143;">V</span><span style="line-height: 1.42857143; font-size: 14px; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">is id error euripidis. Omittam dignissim has cu, mei cu alia persequeris. Solet nominavi assueverit duo et, has ei brute vocent molestiae. Per praesent evertitur in, ei alii convenire est. An aliquid antiopam usu, quando volutpat percipitur in his.</span></h2></div></div>', '2015-05-09 07:03:23'),
(2, 0, 'impressum', '#', 'Impressum', '<h2><span style="font-size: 14px; line-height: 1.42857143; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">© 2015 Tatjana Prijmak</span></h2><p><span style="line-height: 1.42857143;">&nbsp; &nbsp;</span><span style="font-size: 14px; line-height: 1.42857143; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">München 089 - 999 40 999 info@tatjana-prijmak.eu</span></p><p> &nbsp; &nbsp;Tatjana Prijmak</p><h2>L<span style="font-size: 14px; line-height: 1.42857143; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">orem ipsum dolor sit amet, nec viris quaerendum ex, mei no sale ferri constituto. Cu vim consulatu argumentum referrentur, est cu nulla voluptatum. Te apeirian consequat nec. Pro assum munere consequuntur an. Eu est vulputate temporibus accommodare, veniam civibus honestatis quo cu, mel ut soluta eirmod quaestio. Pri probatus recteque salutatus ne, mei ne novum ubique, pri iuvaret efficiantur comprehensam ex.</span></h2><h2>O<span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;">fficiis voluptaria ad eos, nam id cibo simul consectetuer. Mnesarchum sadipscing scribentur eu vel, mea id soluta dicunt. Ex duo harum eligendi, has cu recteque voluptaria. Ius in voluptatum reprimique, quis sint oporteat et vim, liber melius iudicabit ius ei.</span></h2><h2>V<span style="font-size: 14px; line-height: 1.42857143; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">is id error euripidis. Omittam dignissim has cu, mei cu alia persequeris. Solet nominavi assueverit duo et, has ei brute vocent molestiae. Per praesent evertitur in, ei alii convenire est. An aliquid antiopam usu, quando volutpat percipitur in his.</span></h2><h2> E<span style="font-size: 14px; line-height: 1.42857143; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">a lorem mundi nam. No nam debitis consulatu maiestatis, ut pri nostrum dolores. Nec ne alia facete, illud salutandi eloquentiam ad sed. Aliquam impedit nominavi id mei. Est mucius bonorum ad, mea errem similique te, ne iusto aperiri sadipscing has. Cu tota debet per, debet necessitatibus at ius, ludus legimus nec no.</span></h2>', '2015-05-09 07:11:07'),
(3, 0, 'datenschutz', '#', 'Datenschutz', '<h2>L<span style="font-size: 14px; line-height: 1.42857143; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">orem ipsum dolor sit amet, nec viris quaerendum ex, mei no sale ferri constituto. Cu vim consulatu argumentum referrentur, est cu nulla voluptatum. Te apeirian consequat nec. Pro assum munere consequuntur an. Eu est vulputate temporibus accommodare, veniam civibus honestatis quo cu, mel ut soluta eirmod quaestio. Pri probatus recteque salutatus ne, mei ne novum ubique, pri iuvaret efficiantur comprehensam ex.</span></h2><div><h2>O<span style="font-size: 14px; line-height: 1.42857143; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">fficiis voluptaria ad eos, nam id cibo simul consectetuer. Mnesarchum sadipscing scribentur eu vel, mea id soluta dicunt. Ex duo harum eligendi, has cu recteque voluptaria. Ius in voluptatum reprimique, quis sint oporteat et vim, liber melius iudicabit ius ei.</span></h2></div><div><h2>V<span style="font-size: 14px; line-height: 1.42857143; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">is id error euripidis. Omittam dignissim has cu, mei cu alia persequeris. Solet nominavi assueverit duo et, has ei brute vocent molestiae. Per praesent evertitur in, ei alii convenire est. An aliquid antiopam usu, quando volutpat percipitur in his.</span></h2></div><div><h2>E<span style="font-size: 14px; line-height: 1.42857143; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;">a lorem mundi nam. No nam debitis consulatu maiestatis, ut pri nostrum dolores. Nec ne alia facete, illud salutandi eloquentiam ad sed. Aliquam impedit nominavi id mei. Est mucius bonorum ad, mea errem similique te, ne iusto aperiri sadipscing has. Cu tota debet per, debet necessitatibus at ius, ludus legimus nec no.</span></h2></div>', '2015-05-09 06:41:19'),
(4, 21, 'contactPerson', '#', 'ContactPerson', '<h3>Andreas Krais</h3><p><b><i>PR Manager</i></b></p><p><i>contact@t-p.com</i></p><p>+12345678901</p>', '2015-05-09 06:48:55');

-- --------------------------------------------------------

--
-- Table structure for table `Category`
--

CREATE TABLE IF NOT EXISTS `Category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `relatedToUser` int(11) NOT NULL,
  `showInMenu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Category`
--

INSERT INTO `Category` (`id`, `name`, `title`, `relatedToUser`, `showInMenu`) VALUES
(1, 'generic', 'Generic', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Comment`
--

CREATE TABLE IF NOT EXISTS `Comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` int(11) DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5BC96BF023A0E66` (`article`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Document`
--

CREATE TABLE IF NOT EXISTS `Document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL,
  `categoryId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_211FE8209C370B71` (`categoryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `Document`
--

INSERT INTO `Document` (`id`, `name`, `createdAt`, `categoryId`) VALUES
(1, 'jtlshoplogo.png', '2015-03-08 21:11:31', 1),
(5, 'lb2.jpg', '2015-03-13 11:46:05', 1),
(6, 'lb4.jpg', '2015-03-14 10:24:49', 1),
(7, 'ukr-kids.jpg', '2015-03-14 11:16:29', 1),
(8, '01.jpg', '2015-03-19 15:03:12', 1),
(9, '03.jpg', '2015-03-19 15:11:04', 1),
(10, '04.jpg', '2015-03-19 15:13:22', 1),
(11, '480209-2085_1.jpg', '2015-05-09 05:53:57', 1),
(12, '080324-0245_1.jpg', '2015-05-09 05:54:23', 1),
(13, '480261-0014_1.jpg', '2015-05-09 05:54:33', 1),
(14, '073412-0133_1.jpg', '2015-05-09 05:54:44', 1),
(15, '540911-0014_1.jpg', '2015-05-09 05:54:54', 1),
(16, '541352-0014_1.jpg', '2015-05-09 05:55:03', 1),
(17, 'fashion_53.jpg', '2015-05-09 06:25:03', 1),
(18, 'fashion_40.jpg', '2015-05-09 06:32:39', 1),
(19, 'fashion_49.jpg', '2015-05-09 06:32:46', 1),
(20, 'fashion_56.jpg', '2015-05-09 06:32:53', 1),
(21, 'fff.jpg', '2015-05-09 06:44:42', 1),
(22, 'fashion_62.jpg', '2015-05-09 06:55:16', 1),
(23, 'fashion_50.jpg', '2015-05-09 06:56:43', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Event`
--

CREATE TABLE IF NOT EXISTS `Event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `intro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `Event`
--

INSERT INTO `Event` (`id`, `title`, `body`, `intro`, `date`) VALUES
(1, 'Revolution', '', 'Lorem ipsum dolor sit amet, fastidii recusabo ex vix, tacimates inimicus deseruisse id vis, an eos reque melius dissentiet. Ex reque interpretaris pro.', '2015-05-10 00:00:00'),
(2, 'The Fashion', '', 'Lorem ipsum dolor sit amet, fastidii recusabo ex vix, tacimates inimicus deseruisse id vis, an eos reque melius dissentiet.', '2015-05-15 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `Gallery`
--

CREATE TABLE IF NOT EXISTS `Gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` longtext COLLATE utf8_unicode_ci NOT NULL,
  `subheadline` longtext COLLATE utf8_unicode_ci NOT NULL,
  `buttonText` longtext COLLATE utf8_unicode_ci NOT NULL,
  `buttonLink` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` int(11) NOT NULL,
  `identifiedWith` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `Gallery`
--

INSERT INTO `Gallery` (`id`, `headline`, `subheadline`, `buttonText`, `buttonLink`, `image`, `identifiedWith`) VALUES
(1, 'AIR', 'Bald Erhältlich!', 'Online-Shop', 'http://tatjana-prijmak.eu/', 8, 'homeStage'),
(2, 'Sinnlich', 'Neue Kollektion', 'bald erhältlich', 'http://tatjana-prijmak.eu/', 9, 'homeStage'),
(3, 'Weiblich', 'Noch etwas geduld', 'Online-Shop', 'http://tatjana-prijmak.eu/', 10, 'homeStage'),
(4, 'SALE', '20% off', 'Online-Shop', 'http://tatjana-prijmak.eu/', 5, 'homeStage');

-- --------------------------------------------------------

--
-- Table structure for table `GalleryItem`
--

CREATE TABLE IF NOT EXISTS `GalleryItem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `album` int(11) DEFAULT NULL,
  `orderId` int(11) NOT NULL,
  `documentId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_62090D8039986E43` (`album`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Dumping data for table `GalleryItem`
--

INSERT INTO `GalleryItem` (`id`, `title`, `album`, `orderId`, `documentId`) VALUES
(16, 'Folia Dress', 1, 1, 11),
(17, 'Dandridge Shirt Dress', 1, 1, 12),
(18, 'Vaca Striped Dress', 1, 1, 13),
(19, 'Louisa Dress', 1, 1, 14),
(20, 'Claire Tunic', 2, 1, 15),
(21, 'Mirella Dress', 2, 1, 16);

-- --------------------------------------------------------

--
-- Table structure for table `GlobalContent`
--

CREATE TABLE IF NOT EXISTS `GlobalContent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `copy` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `meta` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logoId` int(11) NOT NULL,
  `identifiedWith` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `GlobalContent`
--

INSERT INTO `GlobalContent` (`id`, `title`, `copy`, `meta`, `name`, `address`, `phone`, `email`, `logoId`, `identifiedWith`) VALUES
(1, 'Tatjana Prijmak', '© 2015 Tatjana Prijmak', 'a:2:{i:0;i:1;i:1;i:2;}', 'Tatjana Prijmak', 'München', '089 - 999 40 999', 'info@tatjana-prijmak.eu', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `Message`
--

CREATE TABLE IF NOT EXISTS `Message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `News`
--

CREATE TABLE IF NOT EXISTS `News` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `documents` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `News`
--

INSERT INTO `News` (`id`, `title`, `intro`, `body`, `date`, `documents`) VALUES
(1, 'BELOW CONTENT', 'Lorem mundi nam ea. No nam debitis consulatu maiestatis, ut pri nostrum dolores. Nec ne alia facete, illud salutandi eloquentiam ad sed. Aliquam impedit nominavi id mei.', 'Lorem ipsum dolor sit amet, fastidii recusabo ex vix, tacimates inimicus deseruisse id vis, an eos reque melius dissentiet. Ex reque interpretaris pro. Ad fierent probatus efficiendi vel. Dico doming definitionem eos et. Qui ne choro bonorum nominavi, ad nostro inimicus consequat est, no pertinax mandamus mei.<div><div class="image-container static" data-static="true" data-static-type="image" style="max-width: 1400px; max-height: 788px;"><div class="image" style="padding-bottom: 56.2857142857143%; height: 0px;"><img src="/uploads/generic/fashion_40.jpg" style="max-width: 100%;"></div></div><p></p><p>Nec ex harum homero. No repudiare maiestatis est. Ei odio meliore intellegebat eam, ad inani simul vim. Ne iuvaret disputationi his. Pri ex brute propriae assueverit, pro eu veri viderer. No mollis theophrastus qui, duo lorem ullum nemore ea, ne vel solet tantas ponderum.<br></p></div>', '2015-05-09 06:35:42', 'a:1:{i:0;i:17;}'),
(2, 'Fashion Photography', 'Lorem ipsum dolor sit amet, fastidii recusabo ex vix, tacimates inimicus deseruisse id vis, an eos reque melius dissentiet. Ex reque interpretaris pro. Ad fierent probatus efficiendi vel. Dico doming definitionem eos et.', 'Lorem ipsum dolor sit amet, fastidii recusabo ex vix, tacimates inimicus deseruisse id vis, an eos reque melius dissentiet. Ex reque interpretaris pro. Ad fierent probatus efficiendi vel. Dico doming definitionem eos et. Qui ne choro bonorum nominavi, ad nostro inimicus consequat est, no pertinax mandamus mei.<div><div class="image-container static" data-static="true" data-static-type="image" style="max-width: 1400px; max-height: 788px;"><div class="image" style="padding-bottom: 56.2857142857143%; height: 0px;"><img src="/uploads/generic/fashion_49.jpg" style="max-width: 100%;"></div></div><p></p><p>Nec ex harum homero. No repudiare maiestatis est. Ei odio meliore intellegebat eam, ad inani simul vim. Ne iuvaret disputationi his. Pri ex brute propriae assueverit, pro eu veri viderer. No mollis theophrastus qui, duo lorem ullum nemore ea, ne vel solet tantas ponderum.</p></div>', '2015-05-09 06:58:54', 'a:1:{i:0;i:20;}'),
(3, 'Content on', 'Lorem ipsum dolor sit amet, fastidii recusabo ex vix, tacimates inimicus deseruisse id vis, an eos reque melius dissentiet.', 'Lorem ipsum dolor sit amet, fastidii recusabo ex vix, tacimates inimicus deseruisse id vis, an eos reque melius dissentiet. Ex reque interpretaris pro. Ad fierent probatus efficiendi vel. Dico doming definitionem eos et. Qui ne choro bonorum nominavi, ad nostro inimicus consequat est, no pertinax mandamus mei.<div><div class="image-container static" data-static="true" data-static-type="image" style="max-width: 1400px; max-height: 787px;"><div class="image" style="padding-bottom: 56.2142857142857%; height: 0px;"><img src="/uploads/generic/fashion_50.jpg" style="max-width: 100%;"></div></div><p></p><p>Nec ex harum homero. No repudiare maiestatis est. Ei odio meliore intellegebat eam, ad inani simul vim. Ne iuvaret disputationi his. Pri ex brute propriae assueverit, pro eu veri viderer. No mollis theophrastus qui, duo lorem ullum nemore ea, ne vel solet tantas ponderum.</p></div>', '2015-05-09 06:56:54', 'a:1:{i:0;i:22;}');

-- --------------------------------------------------------

--
-- Table structure for table `Press`
--

CREATE TABLE IF NOT EXISTS `Press` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intro` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `documents` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Tag`
--

CREATE TABLE IF NOT EXISTS `Tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3BC4F16323A0E66` (`article`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Comment`
--
ALTER TABLE `Comment`
  ADD CONSTRAINT `FK_5BC96BF023A0E66` FOREIGN KEY (`article`) REFERENCES `Article` (`id`);

--
-- Constraints for table `Document`
--
ALTER TABLE `Document`
  ADD CONSTRAINT `FK_211FE8209C370B71` FOREIGN KEY (`categoryId`) REFERENCES `Category` (`id`);

--
-- Constraints for table `GalleryItem`
--
ALTER TABLE `GalleryItem`
  ADD CONSTRAINT `FK_62090D8039986E43` FOREIGN KEY (`album`) REFERENCES `Album` (`id`);

--
-- Constraints for table `Tag`
--
ALTER TABLE `Tag`
  ADD CONSTRAINT `FK_3BC4F16323A0E66` FOREIGN KEY (`article`) REFERENCES `Article` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
