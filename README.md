## Test tasks ##
### Integrate a react callendar component in News Edit view ###
1) Go to News page as admin:
http://127.0.0.1:8000/news
2) On the right pane there is Event list. Click on edit button next to any event to trigger an edit mode
3) There is a Date field, currently implemented as a simple text input
Let's improve UX and integrate a react callendar component. 
 
### Make Lookbook images to be draggable. Save an image position via REST API ###
1) Go to Lookbook page as admin:
http://127.0.0.1:8000/lookbook
Each of albums has a title, a description and a list with works. 
2) Click on edit button next to any album. 
There you can add/edit/remove images. Admin should be able to manage the list via a react drag-and-drop and save an image position (orderId) in backend, sothat in default mode the list ist sorted by that parameter.

## Local setup ##
1. go to your project directory
2. git clone https://username@bitbucket.org/lito/test-yurii_venher.git
3. cd test-yurii_venher
4. curl -s http://getcomposer.org/installer | php
5. php composer.phar update
6. create db - tp (db dump is located in root folder - tp.sql)
7. php app/console doctrine:schema:update --force
8. cp -a root/uploads/. web/uploads/ (copy test images)
9. cp -a root/ainojs-react-editor/. src/Lito/FrontendBundle/Resources/public/js/dist/vendor/ainojs-react-editor/ (copy other data)
10. php app/console assets:install --symlink --relative
11. php app/console server:run
12. cd src/Lito/FrontendBundle/Resources/public/js/dist/vendor/ainojs-react-editor/
 a) npm install 
 b) cd node_modules/ 
 c) sudo rm -rf react/
13. back to src/LitoFrontendBundle/Resources/public/js/
14. npm install
15. gulp build