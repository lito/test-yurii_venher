<?php

namespace Lito\ApiBundle\Service;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Lito\ApiBundle\Entity\Article;
use Lito\ApiBundle\Entity\Tag;
use Lito\ApiBundle\Entity\Category;
use Lito\ApiBundle\Service\ContentManagerInterface;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class BlogContentManager implements ContentManagerInterface {
    private $entityManager;
    private $ormRepository; 
    private $authRepository;
    private $documentRepository;
    private $categoryRepository;

    public function __construct(EntityRepository $ormRepository, EntityManager $entityManager, EntityRepository $documentRepository, EntityRepository $categoryRepository, EntityRepository $authContentRepository)
    {
        $this->ormRepository =  $ormRepository;
        $this->entityManager = $entityManager;
        $this->authRepository = $authContentRepository;
        $this->documentRepository = $documentRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function getAll()
    {
        $content = $this->ormRepository->findAll();
        usort($content, function($a, $b) {
            $ad = $a->getDate();
            $bd = $b->getDate();

            if ($ad == $bd) {
              return 0;
            }

            return $ad < $bd ? 1 : -1;
        });
        
        $result = array();
        foreach ($content as $value) {
            $result[] = $this->transformObject($value);
        }
        
        return $result;
    }
    
    public function getAmount($amount) {        
        $content = $this->getAll();
        return array_slice($content, 0, $amount, true);
    }
    
    public function getByCategory($category) {
        $content = $this->getAll();
        foreach($content as $key => $value) {
            if($value['category']->getName() !== $category) {
                unset($content[$key]);
            }
        }
        return $content;
    }
    
    public function getByDate($month, $year) {
        try {
            if(!empty($month) && !empty($year)) {
                $content = $this->getAll();
                foreach ($content as $key => $value) {
                    $articleMonth = $value['date']->format('F');
                    $articleYear =  $value['date']->format('Y');
                    if($articleMonth !== ucfirst($month) || $articleYear !== ucfirst($year)) {
                        unset($content[$key]);
                    }
                }
                return $content;
            } else {
                throw new AccessDeniedHttpException("Access denied. Not enough input parameters");
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function getByTags($tags) {
        $content = $this->getAll();
        if(empty((str_replace(array('[',']'), "", $tags)))) {        
            return $content;
        } else {
            $tags = explode(",", str_replace(array('[',']'), "", $tags));
            $content = $this->getAll();
            $result = array();            
            foreach ($content as $value) {
                $articleTags = $value["tags"];
                if($articleTags) {
                    foreach ($articleTags as $tag) {
                        if(in_array($tag, $tags)) {
                            $result[] = $value;
                            break;
                        }
                    }
                }                
            }            
            return $result;    
        }
    }
   
    public function get($id)
    {
        return $this->ormRepository->find($id);
    }
    
    public function transformObject ($element)
    {        
        $category = $this->categoryRepository->find($element->getCategoryId());
        $result = array(
            "id" => $element->getId(),
            "author" => $element->getAuthor(),
            "title" => $element->getTitle(),
            "body" => $element->getBody(),
            "date" => date_format($element->getDate(), "d.m.Y H:i:s"),
            "category" => $category->getTitle()
        ); 
        
        $documents = unserialize($element->getDocuments());
        if($documents) {
            $images = array();
            foreach ($documents as $id) {
                $document = $this->documentRepository->find($id);
                if($document) {
                    $images[$id]["documentCategoryName"] = $document->getCategory()->getName();
                    $images[$id]["documentName"] = $document->getName();
                }
            }
            $result['images'] = $images;
        } else {
            $result['images'] = array();
        }
        
        $tags = $element->getTags();
        $tagsTitles = array();
        foreach ($tags as $tag) {
            $tagsTitles[] = $tag->getTitle();
        }
        $result["tags"] = $tagsTitles;
        
        $comments = $element->getComments();
        $articleComments = array();
        foreach ($comments as $key => $value) {
            $articleComments[$key]["author"] = $value->getAuthor();
            $articleComments[$key]["body"] = $value->getBody();
            $articleComments[$key]["date"] = $value->getDate();
        }
        $result["comments"] = $articleComments;
        
        return $result;
    }

    public function set($content, $data)
    {        
        // TBD Add automati setter if a key exists
        $content->setAuthor($data["author"]);
        $content->setTitle($data["title"]);
        $content->setBody($data["body"]);
        if($data['documents'][0]) {
            $content->setDocuments(serialize($data['documents']));
        } elseif(!unserialize($content->getDocuments())) {            
            $content->setDocuments(serialize(array()));
        }
        $category = $this->categoryRepository->findBy(array('id' => $content->getCategoryId()));
        if(!empty($category)) {
            $category = $category[0];
            $category->setName($data['category']);
            $category->setTitle($data['category']);
            $category->setRelatedToUser(0);
            $category->setShowInMenu(0);            
        } else {
            $category = new Category();
            $category->setName($data['category']);
            $category->setTitle($data['category']);
            $category->setRelatedToUser(0);
            $category->setShowInMenu(0);  
            $this->entityManager->persist($category);
            $this->entityManager->flush();
        }
        $content->setCategoryId($category->getId());
        if(isset($data["tags"])) {
            $tags = $content->getTags();
            if($tags) {
               foreach ($tags as $tag) {                
                    $content->removeTag($tag);
                    $this->entityManager->remove($tag);
                }  
            }
                   
            foreach ($data["tags"] as $tagData) {
                $newTag = new Tag();            
                $newTag->setTitle($tagData["title"]);
                $newTag->setArticle($content);
                $content->addTags($newTag);
                $this->entityManager->persist($newTag);
            }
        } 
        
        $content->setDate(new \DateTime('now'));

        return $content;
    }
    
    public function updateById ($id, $data) {
        $element = $this->get($id);
        if(!$element) {
            // TBD add exception
        }
        
        $this->save($this->set($element, $data));
        return $this->transformObject($element);
    }

    public function delete($id)
    {
        $content = $this->get($id);
        $this->entityManager->remove($content);
        $this->entityManager->flush();
        
        return array(
            "success" => true,
            "message" => "Object ".$id." was successfully removed"
        );
        
    }
    
    public function add ($element)
    {
        $content = new Article();
        $this->save($this->set($content, $element));
        
        return $this->transformObject($content);
        
    }
    
    private function save(Article $article)
    {
        $this->entityManager->persist($article);
        $this->entityManager->flush();
    }
    
    public function addByToken($element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->add($element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function deleteByToken($id, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));                
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->delete($id);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function updateByToken($id, $element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->updateById($id, $element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
}
