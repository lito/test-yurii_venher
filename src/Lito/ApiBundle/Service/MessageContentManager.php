<?php

namespace Lito\ApiBundle\Service;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Lito\ApiBundle\Service\ContentManagerInterface;
use Lito\ApiBundle\Entity\Message;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class MessageContentManager implements ContentManagerInterface
{	
    private $entityManager;
    private $ormRepository;
    private $authRepository;

    public function __construct(EntityRepository $ormRepository, EntityManager $entityManager, EntityRepository $authContentRepository)
    {
        $this->ormRepository =  $ormRepository;
        $this->entityManager = $entityManager;
        $this->authRepository = $authContentRepository;
    }

    public function getAll()
    {
        return $this->ormRepository->findAll();
    }
   
    public function get($id)
    {        

        return $this->ormRepository->find($id);
    }

    public function set($message, $data)
    {
        // TBD Add automatic setter if a key exists
        $message->setName($data["name"]);
        $message->setEmail($data["email"]);
        $message->setMessage($data["textarea"]);
        $message->setDate(new \DateTime('now'));
        
        return $message;
    }
    
    public function updateById ($id, $data) {
        $element = $this->get($id);
        if(!$element) {
            // TBD add exception
        }
        
        $this->save($this->set($element, $data));
        return $element;
    }

    public function delete($id)
    {

        $this->entityManager->remove($this->get($id));
        $this->entityManager->flush();

        return array(
            "success" => true
        );
    }
    
    public function add ($element)
    {
        if(!in_array("", $element)) {
            
            $message = new Message();
            $this->save($this->set($message, $element));

            return $message->getId();
            
        } else {
            
            return FALSE;
            
        }
        
    }
    
    private function save(Message $message)
    {
        $this->entityManager->persist($message);
        $this->entityManager->flush();
    }
	
    public function getJsonObject($element) {
				
        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
	$message = $this->get($this->add($element));
        $message->setDate(($message->getDate()->format('d/m/Y H:i:s')));
        
	try {
            
            $jsonContent = $serializer->serialize($message, 'json');
                    
            if(!isset($message) OR json_last_error() > 0) {
        
                throw new AccessDeniedHttpException('Access denied');

            }
            

            return $jsonContent;

	} catch (\Exception $e) {

            return $serializer->serialize(array(
				'error' => array('code' => $e->getStatusCode(), 'message' => $e->getMessage())), 'json');
        
            
        }
    }
    
    public function addByToken($element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->add($element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function deleteByToken($id, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));                
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->delete($id);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function updateByToken($id, $element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->updateById($id, $element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
}

?>
