<?php

namespace Lito\ApiBundle\Service;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Lito\ApiBundle\Entity\Event;
use Lito\ApiBundle\Service\ContentManagerInterface;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class EventsContentManager implements ContentManagerInterface {    
    private $entityManager;
    private $ormRepository; 
    private $authRepository;
    private $albumRepository;

    public function __construct(EntityRepository $ormRepository, EntityManager $entityManager, EntityRepository $albumRepository, EntityRepository $authContentRepository)
    {
        $this->ormRepository =  $ormRepository;
        $this->entityManager = $entityManager;
        $this->authRepository = $authContentRepository;
        $this->albumRepository = $albumRepository;
    }

    public function getAll()
    {
        $content = $this->ormRepository->findAll();
        usort($content, function($a, $b) {
            $ad = $a->getDate();
            $bd = $b->getDate();

            if ($ad == $bd) {
              return 0;
            }

            return $ad < $bd ? -1 : 1;
        });        
        
        foreach ($content as $key => $event) {
            if($event->getDate() < new \DateTime()) {
                unset($content[$key]);
            }
        }
        $events = array();
        foreach($content as $event) {
            $events[] = $this->transformObject($event);
        }
        
        return $events;
    }
    
    public function getAmount($amount) {        
        $content = $this->getAll();
        return array_slice($content, 0, $amount, true);
    }
   
    public function get($id)
    {
        return $this->ormRepository->find($id);
    }
    
    public function transformObject ($element)
    {
        $result = array(
            "id" => $element->getId(),
            "title" => $element->getTitle(),
            "body" => $element->getBody(),
            "intro" => $element->getIntro(),
            "date" => date_format($element->getDate(), "d.m.Y H:i:s")
        ); 
        
        return $result;
    }

    public function set($content, $data)
    {
        // TBD Add automati setter if a key exists
        $content->setTitle($data["title"]);
        $content->setIntro($data["intro"]);
        if(isset($data['body'])) {
            $content->setBody($data["body"]);
        } else {
            $content->setBody("");
        } 
        $date = date_create($data['date']);
        $content->setDate($date);      

        return $content;
    }
    
    public function updateById ($id, $data) {
        $element = $this->get($id);
        if(!$element) {
            // TBD add exception
        }
        
        $this->save($this->set($element, $data));
        return $this->transformObject($element);
    }

    public function delete($id)
    {
        $content = $this->get($id);
        $this->entityManager->remove($content);
        $this->entityManager->flush();
        
        return array(
            "success" => true,
            "message" => "Object ".$id." was successfully removed"
        );
        
    }
    
    public function add ($element)
    {
        $content = new Event();
        $this->save($this->set($content, $element));
        
        return $this->transformObject($content);
        
    }
    
    private function save(Event $event)
    {
        $this->entityManager->persist($event);
        $this->entityManager->flush();
    }
    
    public function addByToken($element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->add($element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function deleteByToken($id, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));                
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->delete($id);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function updateByToken($id, $element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->updateById($id, $element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
}
