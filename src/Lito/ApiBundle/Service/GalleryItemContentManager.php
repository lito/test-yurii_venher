<?php

namespace Lito\ApiBundle\Service;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Lito\ApiBundle\Entity\GalleryItem;
use Lito\ApiBundle\Service\ContentManagerInterface;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class GalleryItemContentManager implements ContentManagerInterface {
    private $entityManager;
    private $ormRepository; 
    private $authRepository;
    private $albumRepository;
    private $documentRepository;

    public function __construct(EntityRepository $ormRepository, EntityManager $entityManager, EntityRepository $albumRepository, EntityRepository $authContentRepository, EntityRepository $documentRepository)
    {
        $this->ormRepository =  $ormRepository;
        $this->entityManager = $entityManager;
        $this->authRepository = $authContentRepository;
        $this->albumRepository = $albumRepository;
        $this->documentRepository = $documentRepository;
    }

    public function getAll()
    {
        $content = $this->ormRepository->findAll();
        $result = array();
        foreach ($content as $value) {
            $result[] = $this->transformObject($value);
        }
        
        return $result;
    }
   
    public function get($id)
    {
        return $this->ormRepository->find($id);
    }

    public function set($content, $data)
    {
        // TBD Add automati setter if a key exists
        $content->setTitle($data["title"]);
        $content->setDocumentId($data["documentId"]);
        if(!is_null($this->albumRepository->find($data["album"]))) {
            $content->setAlbum($this->albumRepository->find($data["album"]));
        }
        $content->setOrderId($data["orderId"]);        

        return $content;
    }
    
    public function transformObject($element) {
        
        $result = array(
            'id' => $element->getId(),
            'title' => $element->getTitle(),
            'orderId' => $element->getOrderId()
        );
        $document = $this->documentRepository->find($element->getDocumentId());
        if($document) {            
            $result['image'] = array(
                'documentId' => $document->getId(),
                'documentCategoryName' => $document->getCategory()->getName(),
                'documentName' => $document->getName()
            );
        }
        
        return $result;
        
    }
    
    public function updateById ($id, $data) {
        $element = $this->get($id);
        if(!$element) {
            // TBD add exception
        }
        
        $this->save($this->set($element, $data));
        return $this->transformObject($element);
    }

    public function delete($id)
    {
        $content = $this->get($id);
        $this->entityManager->remove($content);
        $this->entityManager->flush();
        
        return array(
            "success" => true,
            "message" => "Object ".$id." was successfully removed"
        );
        
    }
    
    public function add ($element)
    {
        $content = new GalleryItem();
        $this->save($this->set($content, $element));
        
        return $this->transformObject($content);
        
    }
    
    private function save(GalleryItem $galleryItem)
    {
        $this->entityManager->persist($galleryItem);
        $this->entityManager->flush();
    }
    
    public function addByToken($element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->add($element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function deleteByToken($id, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));                
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->delete($id);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function updateByToken($id, $element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->updateById($id, $element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
}
