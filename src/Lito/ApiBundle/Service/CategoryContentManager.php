<?php

namespace Lito\ApiBundle\Service;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Lito\ApiBundle\Service\ContentManagerInterface;
use Lito\ApiBundle\Entity\Category;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class CategoryContentManager implements ContentManagerInterface
{
    private $entityManager;
    private $ormRepository;    
    private $authRepository;

    public function __construct(EntityRepository $categoryContentRepository, EntityManager $entityManager, EntityRepository $authContentRepository)
    {
        $this->ormRepository = $categoryContentRepository;
        $this->entityManager = $entityManager;
        $this->authRepository= $authContentRepository;
    }
    
    public function getAll()
    {
        return $this->ormRepository->findAll();
    }

    public function getByIdentifier($identifier)
    {
        return $this->ormRepository->findBy(
                array("identifiedWith" => $identifier)
        );
    }

    public function get($id)
    {
        // TBD through exception if object is not found 
        return $this->ormRepository->find($id);
    }

    public function set($content, $data)
    {
        $content->setName($data["name"]);
        $content->setTitle($data["title"]);
        if(isset($data["relatedToUser"])) {
            $content->setRelatedToUser($data["relatedToUser"]);    
        }

        if(isset($data["showInMenu"])) {
            $content->setShowInMenu($data["showInMenu"]);    
        }
        
        return $content;
    }

    public function delete($id)
    {
        $this->entityManager->remove($this->get($id));
        $this->entityManager->flush();
        return array(
            "success" => true
        );
    }
    
    public function add($element)
    {
        $category = new Category();
        $this->save($this->set($category, $element));
        
        return $category;
    }
    
    public function updateById($id, $data)
    {
        $element = $this->get($id);
        $this->save($this->set($element, $data));
        return $element;
    }
    
    private function save(Category $category) {
        $this->entityManager->persist($category);
        $this->entityManager->flush();
    }
    
    public function addByToken($element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->add($element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function deleteByToken($id, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));                
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->delete($id);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function updateByToken($id, $element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->updateById($id, $element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
}