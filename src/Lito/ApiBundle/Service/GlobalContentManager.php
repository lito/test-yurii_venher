<?php

namespace Lito\ApiBundle\Service;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Lito\ApiBundle\Entity\GlobalContent;
use Lito\ApiBundle\Service\ContentManagerInterface;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class GlobalContentManager implements ContentManagerInterface
{   
    private $entityManager;
    private $ormRepository;
    private $documentRepository;  
    private $authRepository;

    public function __construct(EntityRepository $ormRepository, EntityManager $entityManager, EntityRepository $documentRepository, EntityRepository $authContentRepository)
    {
        $this->ormRepository =  $ormRepository;
        $this->entityManager = $entityManager;
        $this->documentRepository = $documentRepository;
        $this->authRepository= $authContentRepository;
    }

    public function getAll()
    {
        $results = array();
        $content = $this->ormRepository->findAll();
       
        if(count($content)) {
            
            foreach ($content as $element) {
                
                $results[] = $this->transformObject($element);
                
            }
        
        }
        return $results;
    }
    
    public function transformObject($element)
    {
        $result = array(
            "id" => $element->getId(),
            "title" => $element->getTitle(),
            "copy" => $element->getCopy(),
            "meta" => $element->getMeta(),
            "name" => $element->getName(),
            "address" => $element->getAddress(),
            "phone" => $element->getPhone(),
            "email" => $element->getEmail(),
            "logoId" => $element->getLogoId(),
            "identifiedWith" => $element->getIdentifiedWith()
        );
        
        $document = $this->documentRepository->find($element->getLogoId()); 
        if($document) {
            $category = $document;
            $result["logoCategoryName"] = $document->getCategory()->getName();
            $result["logoDocumentName"] = $document->getName();
        }
        return $result;
    }
    
    public function getByIdentifier($identifier)
    {
        $results = array();
        $content = $this->ormRepository->findBy(
                array("identifiedWith" => $identifier));

       
        if(count($content)) {
            
            foreach ($content as $element) {
                
                $results[] = $this->transformObject($element);
                
            }
        
        }
        return $results;
    }
   
    public function get($id)
    {
        return $this->ormRepository->find($id);
    }

    public function set($content, $data)
    {
        // TBD Add automati setter if a key exists
        $content->setTitle($data["title"]);
        $content->setCopy($data["copy"]);
        $content->setMeta($data["meta"]);
        $content->setName($data["name"]);
        $content->setAddress($data["address"]);
        $content->setPhone($data["phone"]);
        $content->setEmail($data["email"]);
        $content->setLogoId($data["logoId"]);
        $content->setIdentifiedWith($data["identifiedWith"]);
        return $content;
    }
    
    public function updateById ($id, $data) {
        $element = $this->get($id);
        
        if(!$element) {
            // TBD add exception
        }
        
        $this->save($this->set($element, $data));
        
        return $this->transformObject($element);
    }

    public function delete($id)
    {
        $content = $this->get($id);
        $this->entityManager->remove($content);
        $this->entityManager->flush();
        
        return array(
            "success" => true,
            "message" => "Object ".$id." was successfully removed"
        );
    }
    
    public function add ($element)
    {
        $content = new GlobalContent();
        $this->save($this->set($content, $element));
        
        return $content->getId();
        
    }
    
    private function save(GlobalContent $globalContent)
    {
        $this->entityManager->persist($globalContent);
        $this->entityManager->flush();
    } 
    
    public function addByToken($element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->add($element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function deleteByToken($id, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));                
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->delete($id);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function updateByToken($id, $element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->updateById($id, $element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
}