<?php

namespace Lito\ApiBundle\Service;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Lito\ApiBundle\Entity\News;
use Lito\ApiBundle\Service\ContentManagerInterface;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class NewsContentManager implements ContentManagerInterface {
    
    private $entityManager;
    private $ormRepository; 
    private $authRepository;
    private $documentRepository;

    public function __construct(EntityRepository $ormRepository, EntityManager $entityManager, EntityRepository $authContentRepository, EntityRepository $documentRepository)
    {
        $this->ormRepository =  $ormRepository;
        $this->entityManager = $entityManager;
        $this->authRepository = $authContentRepository;
        $this->documentRepository = $documentRepository;
    }

    public function getAll()
    {
        $content = $this->ormRepository->findAll();
        usort($content, function($a, $b) {
            $ad = $a->getDate();
            $bd = $b->getDate();

            if ($ad == $bd) {
              return 0;
            }

            return $ad < $bd ? 1 : -1;
        });
        
        $result = array();
        foreach ($content as $value) {
            $result[] = $this->transformObject($value);
        }
        
        return $result;
    }
    
    public function getAmount($amount) {        
        $content = $this->getAll();
        return array_slice($content, 0, $amount, true);
    }
    
    public function getByRoutingName($routingName) {
        $content = $this->getAll();
        foreach ($content as $value) {
            if(str_replace(" ", "-", mb_strtolower($value['title'], 'UTF-8')) === $routingName) {
                return $value;
            }
        }
        return $content;
    }
   
    public function get($id)
    {
        return $this->ormRepository->find($id);
    }
    
    public function transformObject ($element)
    {
        $result = array(
            "id" => $element->getId(),
            "title" => $element->getTitle(),
            "body" => $element->getBody(),
            "intro" => $element->getIntro(),
            "date" => date_format($element->getDate(), "d.m.Y H:i:s"),
            "routingName" => str_replace(" ", "-", mb_strtolower($element->getTitle(), 'UTF-8'))
        ); 
        $documents = unserialize($element->getDocuments());
        if($documents) {
            $images = array();
            foreach ($documents as $id) {
                if($id) {
                    $document = $this->documentRepository->find($id);
                    if($document) {
                        $images[$id]["documentId"] = $document->getId();
                        $images[$id]["documentCategoryName"] = $document->getCategory()->getName();
                        $images[$id]["documentName"] = $document->getName();
                    }
                }
                
            }
            $images2 = array();
            foreach ($images as $value) {
                $images2[] = $value;
            }
            $result['images'] = $images2;
        } else {
            $result['images'] = array();
        }        
        return $result;
    }

    public function set($content, $data)
    {
        // TBD Add automati setter if a key exists
        $content->setTitle($data["title"]);
        $content->setIntro($data["intro"]);
        if(isset($data['body'])) {
            $content->setBody($data["body"]);
        } elseif(!$content->getBody()) {
                $content->setBody("");
        }
        if($data['documents'][0]) {
            $content->setDocuments(serialize($data['documents']));
        } elseif(!unserialize($content->getDocuments())) {            
            $content->setDocuments(serialize(array()));
        }
        
        $content->setDate(new \DateTime('now'));

        return $content;
    }
    
    public function updateById ($id, $data) {
        $element = $this->get($id);
        if(!$element) {
            // TBD add exception
        }
        
        $this->save($this->set($element, $data));
        return $this->transformObject($element);
    }

    public function delete($id)
    {
        $content = $this->get($id);
        $this->entityManager->remove($content);
        $this->entityManager->flush();
        
        return array(
            "success" => true,
            "message" => "Object ".$id." was successfully removed"
        );
        
    }
    
    public function add ($element)
    {
        $content = new News();
        $this->save($this->set($content, $element));
        
        return $this->transformObject($content);
        
    }
    
    private function save(News $news)
    {
        $this->entityManager->persist($news);
        $this->entityManager->flush();
    }
    
    public function addByToken($element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->add($element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function deleteByToken($id, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));                
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->delete($id);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
    
    public function updateByToken($id, $element, $token) {
        try {
            if(!empty($token)) {
                $authContent = $this->authRepository->findBy(array('token' => $token));
                if(!empty($authContent) && $authContent[0]->getIsAdmin() === true) {
                    return $this->updateById($id, $element);                    
                } else {
                    throw new AccessDeniedHttpException('Access denied. Wrong token');
                }
            } else {
                throw new UnauthorizedHttpException('/api/contents','Bad request. Not enough input parameters');
            }
        } catch (Exception $ex) {
            $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
            return $serializer->serialize(array(
                                            'error' => array('code' => $ex->getStatusCode(), 'message' => $ex->getMessage())), 'json');
        }
    }
}
