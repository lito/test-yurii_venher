<?php

namespace Lito\ApiBundle\Controller;

use FOS\RestBundle\Util\Codes;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\RouteRedirectView;

use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Rest controller for content elements
 *
 * @package Lito\ApiBundle\Controller
 * @author Roman <people@lito-pro.de>
 */
class ContentController extends FOSRestController
{
    /**
     * return \Lito\ApiBundle\ContentManager
     */
    public function getContentManager($contentType = "base")
    {
        // TBD call a service based on content type
        return $this->get('lito_api.'.$contentType.'_content_manager');
    }

    /**
     * List all content elements.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="identifier", nullable=true, description="identified with page or section")
     * @Annotations\QueryParam(name="type", nullable=true, description="has or of the predifined content types")  
     * @Annotations\QueryParam(name="category", nullable=true, description="defines category to select articles by")   
     * @Annotations\QueryParam(name="month", nullable=true, description="defines month to select articles by (parameter year must be set)")
     * @Annotations\QueryParam(name="year", nullable=true, description="defines year to select articles by (parameter month must be set)")
     * @Annotations\QueryParam(name="tags", nullable=true, description="defines tags to select by (must be an array)")
     * @Annotations\QueryParam(name="article", nullable=true, description="defines article to get comments for")
     * @Annotations\QueryParam(name="filterbyrouting", nullable=true, description="defines title to get news by")
     * 
     * @Annotations\View()
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getContentsAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $identifier = $paramFetcher->get('identifier');
        $type = $paramFetcher->get('type');
        $category = $paramFetcher->get('category');
        $month = $paramFetcher->get('month');
        $year = $paramFetcher->get('year');
        $tags = $paramFetcher->get('tags');
        $article = $paramFetcher->get('article');
        $routingName = $paramFetcher->get('filterbyrouting');
        $cm = $this->getContentManager($type);
        if($type === 'blog') {
            if(isset($category)) {
                return $cm->getByCategory($category);
            }
            if(isset($month) && isset($year)) {
                return $cm->getByDate($month, $year);
            }
            if(isset($tags)) {
                return $cm->getByTags($tags);
            }            
        }
        
        if($type === 'comment') {
            if(isset($article)) {
                return $cm->getByArticle($article);
            }
        }
        
        if($type === 'news') {
            if(isset($routingName)) {
                return $cm->getByRoutingName($routingName);
            }
        }
        
        if(isset($identifier)){
            return $cm->getByIdentifier($identifier);
        }
        return $cm->getAll();
    }
    
    /**
     * Update content element.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="type", nullable=true, description="has or of the predifined content types")
     * @Annotations\QueryParam(name="token", nullable=true, description="defines admin or user session")
     * 
     * @Annotations\View()
     *
     * @param Request               $request      the request object
     * @param int $id content element id
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */ 
    public function putContentAction(Request $request, $id, ParamFetcherInterface $paramFetcher) {
        $type = $paramFetcher->get('type');
        $content = json_decode($request->getContent(), true);
        $token = $paramFetcher->get('token');
               
        return $this->getContentManager($type)->updateByToken($id, $content, $token);
    }
    
    /**
     * Create new content element.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     * 
     * @Annotations\QueryParam(name="type", nullable=true, description="has or of the predifined content types")
     * @Annotations\QueryParam(name="token", nullable=true, description="defines admin or user session")
     *
     * @Annotations\View(templateVar="content")
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function postContentAction(Request $request, ParamFetcherInterface $paramFetcher) {
        $type = $paramFetcher->get('type');        
        $content = json_decode($request->getContent(), true);
        $token = $paramFetcher->get('token');
        
        //return $type;
        // TBD implement add method for all services
        return $this->getContentManager($type)->addByToken($content, $token);

    }


    /**
     * Remove content element.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="type", nullable=true, description="has or of the predifined content types")
     * @Annotations\QueryParam(name="token", nullable=true, description="defines admin or user session")
     * 
     * @Annotations\View()
     *
     * @param Request               $request      the request object
     * @param int $id content element id
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */ 
    public function deleteContentAction(Request $request, $id, ParamFetcherInterface $paramFetcher) {
        $type = $paramFetcher->get('type');
        $token = $paramFetcher->get('token');
        
        return $this->getContentManager($type)->deleteByToken($id, $token);
    }
    
}
