<?php

namespace Lito\ApiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\Controller\FOSRestController;

class ContactFormController extends FOSRestController
{
    public function getMessageManager() {
		
	return $this->get('lito_api.message_content_manager');
		
    }
	
    public function getMessageAction(Request $request) {
		
	return json_decode($request->getContent(), true);
    }
	
    public function postMessageAction(Request $request) {
		
	$message = $this->getMessageAction($request);
	return $this->getMessageManager()->getJsonObject($message);
    }
	
    public function getEmailManager() {
		
	return $this->get('lito_api.email_manager');
		
    }
	
    public function sendMessageAction(Request $request) {
		
	$message = $this->getMessageAction($request);
	return $this->getEmailManager()->sendEmail($message);
		
    }
    /**
     * @Route("api/contact/form")
     * @Template()
     */
    public function contactFormAction(Request $request)
    {
		
	$this->sendMessageAction($request);		
	return new Response($this->postMessageAction($request));		
		
    }

}
