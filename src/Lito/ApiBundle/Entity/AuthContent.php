<?php

namespace Lito\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AuthContent
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class AuthContent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=32)
     */
    private $token;

    /**
     * @var integer
     *
     * @ORM\Column(name="relatedToUser", type="integer")
     */
    private $relatedToUser;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isAdmin", type="boolean")
     */
    private $isAdmin;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return AuthContent
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set relatedToUser
     *
     * @param integer $relatedToUser
     * @return AuthContent
     */
    public function setRelatedToUser($relatedToUser)
    {
        $this->relatedToUser = $relatedToUser;

        return $this;
    }

    /**
     * Get relatedToUser
     *
     * @return integer 
     */
    public function getRelatedToUser()
    {
        return $this->relatedToUser;
    }

    /**
     * Set isAdmin
     *
     * @param boolean $isAdmin
     * @return AuthContent
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * Get isAdmin
     *
     * @return boolean 
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }
    
}
