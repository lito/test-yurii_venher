<?php

namespace Lito\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Album
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Album
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="identifiedWith", type="string", length=255)
     */
    private $identifiedWith;

    /**
     * @var string
     *
     * @ORM\Column(name="intro", type="string", length=255)
     */
    private $intro;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string", length=255)
     */
    private $body;
    
    /**
     * @var string
     *
     * @ORM\Column(name="link", type="text")
     */
    private $link;

    /**
     * @ORM\OneToMany(targetEntity="GalleryItem", mappedBy="album", cascade={"persist", "remove"})
     */
    private $items;
    
    public function __construct() {
        $this->items = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Album
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Set identifiedWith
     *
     * @param string $identifiedWith
     * @return BaseContent
     */
    public function setIdentifiedWith($identifiedWith)
    {
        $this->identifiedWith = $identifiedWith;
    
        return $this;
    }

    /**
     * Get identifiedWith
     *
     * @return string 
     */
    public function getIdentifiedWith()
    {
        return $this->identifiedWith;
    }

    /**
     * Set intro
     *
     * @param string $intro
     * @return Album
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;

        return $this;
    }

    /**
     * Get intro
     *
     * @return string 
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Album
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }
    
    /**
     * Set link
     *
     * @param string $link
     * @return BaseContent
     */
    public function setLink($link)
    {
        $this->link = $link;
    
        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Add items
     *
     * @param \Lito\ApiBundle\Entity\GalleryItem $item
     * @return Album
     */
    public function addItems(\Lito\ApiBundle\Entity\GalleryItem $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove items
     *
     * @param \Lito\ApiBundle\Entity\GalleryItem $item
     */
    public function removeItem(\Lito\ApiBundle\Entity\GalleryItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }
}
