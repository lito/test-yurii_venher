<?php

namespace Lito\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlobalContent
 *
 * @ORM\Table()
 * @ORM\Entity
 *  
 */
class GlobalContent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="copy", type="text", length=255)
     */
    private $copy; 
    
    /**
     * @var array
     *
     * @ORM\Column(name="meta", type="array")
     */
    private $meta;

    /**
     * @var string
     * 
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;  
    
    /**
     * @var string
     * 
     * @ORM\Column(name="identifiedWith", type="string", length=255)
     */
    private $identifiedWith;  
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="logoId", type="integer")
     */
    private $logoId;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return GlobalContent
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }    

    /**
     * Set copy
     *
     * @param string $copy
     * @return GlobalContent
     */
    public function setCopy($copy)
    {
        $this->copy = $copy;
    
        return $this;
    }

    /**
     * Get copy
     *
     * @return string 
     */
    public function getCopy()
    {
        return $this->copy;
    }    
    
    /**
     * Set meta
     *
     * @param array $meta
     * @return GlobalContent
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;

        return $this;
    }

    /**
     * Get meta
     *
     * @return array
     */
    public function getMeta()
    {
        return $this->meta;
    }

/**
     * Set name
     *
     * @param string $name
     * @return GlobalContent
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * Set address
     *
     * @param string $address
     * @return GlobalContent
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress() {
        return $this->address;
    }
    
    /**
     * Set phone
     *
     * @param string $phone
     * @return GlobalContent
     */
    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone() {
        return $this->phone;
    }
    
    /**
     * Set email
     *
     * @param string $email
     * @return GlobalContent
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }
    
    /**
     * Get logoId
     *
     * @return integer 
     */
    public function getLogoId()
    {
        return $this->logoId;
    }
    
    /**
     * Set logoId
     *
     * @param integer $logoId
     * @return GlobalContent
     */
    public function setLogoId($logoId) {
        $this->logoId = $logoId;

        return $this;
    }
    
    /**
     * Set identifiedWith
     *
     * @param string $identifiedWith
     * @return GlobalContent
     */
    public function setIdentifiedWith($identifiedWith)
    {
        $this->identifiedWith = $identifiedWith;
    
        return $this;
    }

    /**
     * Get identifiedWith
     *
     * @return string 
     */
    public function getIdentifiedWith()
    {
        return $this->identifiedWith;
    }
}
