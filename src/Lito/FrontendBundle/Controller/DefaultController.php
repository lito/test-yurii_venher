<?php

namespace Lito\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('LitoFrontendBundle:Default:index.html.twig');
    }
}
