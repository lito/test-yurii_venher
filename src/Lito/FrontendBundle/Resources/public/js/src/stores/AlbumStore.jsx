var _ = require('underscore');

var Fluxxor = require("fluxxor");

var ContentConstants = require('../constants/ContentConstants');

var AlbumStore = Fluxxor.createStore({
  initialize: function() {
    this.loading = false;
    this.error = false;
    this.albums = [];

    this.bindActions(
      ContentConstants.LOAD_ALBUMS, this.onLoadAlbums,
      ContentConstants.LOAD_ALBUMS_SUCCESS, this.onLoadAlbumsSuccess,
      ContentConstants.LOAD_ALBUMS_FAIL, this.onLoadAlbumsFail,

      ContentConstants.ADD_ALBUM, this.onAddAlbum,
      ContentConstants.ADD_ALBUM_SUCCESS, this.onAddAlbumSuccess,
      ContentConstants.ADD_ALBUM_FAIL, this.onAddAlbumFail,

      ContentConstants.EDIT_ALBUM, this.onEditAlbum,
      ContentConstants.EDIT_ALBUM_SUCCESS, this.onEditAlbumSuccess,
      ContentConstants.EDIT_ALBUM_FAIL, this.onEditAlbumFail,

      ContentConstants.REMOVE_ALBUM, this.onRemoveAlbum,
      ContentConstants.REMOVE_ALBUM_SUCCESS, this.onRemoveAlbumSuccess,
      ContentConstants.REMOVE_ALBUM_FAIL, this.onRemoveAlbumFail
    );
  },

    onLoadAlbums: function() {
      this.loading = true;
      this.error = null;
      this.emit("change");
    },

    onLoadAlbumsSuccess: function(data) {
      this.loading = false;
      this.albums = data;
      this.emit("change");
    },

    onLoadAlbumsFail: function() {
      this.loading = false;
      this.error = true;
      this.emit("change");
    },

    onAddAlbum() {
      this.loading = true;
      this.emit("change");
    },

    onAddAlbumSuccess(data) {
      this.loading = true;
      this.emit("change");
      this.loading = false;
      this.albums.push(data);
      this.emit("change");
    },

    onAddAlbumFail() {
      this.loading = false;
      this.error = true;
      this.emit("change");
    },

    onEditAlbum() {
      this.loading = true;
      this.emit("change");
    },

    onEditAlbumSuccess(data) {
      this.loading = false;
      this.albums = _.map(this.albums, function (obj) {
        if(data.id === obj.id){
      
          obj = data;
        }
        return obj;
      });

      this.emit("change");
      
    },

    onEditAlbumFail(id) {
      this.loading = false;
      this.removingPending = id;
      this.emit("change");
      
    },

    onRemoveAlbum(id) {
      this.loading = false;
      this.removingPending = id;
      this.emit("change");
      
    },

    onRemoveAlbumSuccess(id) {
      this.loading = false;
      this.albums = _.without(this.albums, _.findWhere(this.albums, {id: this.removingPending}));
      this.emit("change");
    },

    onRemoveAlbumFail() {
      this.loading = false;
      this.error = true;
      this.removingPending = null;
      this.emit("change");
    }

  })

module.exports = AlbumStore;