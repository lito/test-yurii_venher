var Fluxxor = require("fluxxor");

var ContentConstants = require('../constants/ContentConstants');

var BaseContentStore = Fluxxor.createStore({
  initialize: function() {
    this.loading = false;
    this.error = false;
    this.contentList = [];
    
    this.bindActions(
      ContentConstants.LOAD_CONTENT_BASE, this.onLoad,
      ContentConstants.LOAD_CONTENT_BASE_SUCCESS, this.onLoadSuccess,
      ContentConstants.LOAD_CONTENT_BASE_FAIL, this.onLoadFail,
      ContentConstants.EDIT_CONTENT_BASE, this.onEdit,
      ContentConstants.EDIT_CONTENT_BASE_SUCCESS, this.onEditSuccess,
      ContentConstants.EDIT_CONTENT_BASE_FAIL, this.onEditFail
    );
  },

  setAttributes: function(updatedObj, identifier) {
    this.contentList[identifier] = _.map(this.contentList[identifier], function (obj) {
      if(obj.id === updatedObj.id){
        obj = updatedObj;
      }
      return obj;
    });
    
  },

  onLoad: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onLoadSuccess: function(response) {
    this.loading = false;
    this.contentList[response.identifier] = response.data;
    
    this.emit("change");
  },

  onLoadFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onEdit: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onEditSuccess: function(response) {
    this.loading = false;
    this.setAttributes(response.data["element"], response.identifier);
    this.emit("change");
  },

  onEditFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  }
});

module.exports = BaseContentStore;