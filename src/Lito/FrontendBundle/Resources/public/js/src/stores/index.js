var GlobalSettingsStore = require('./GlobalSettingsStore'),
	CategoryStore = require('./CategoryStore'),
	UserStore = require('./UserStore'),
	BaseContentStore = require('./BaseContentStore'),
	GalleryStore = require('./GalleryStore'),
    AlbumStore = require('./AlbumStore'),
    NewsStore = require('./NewsStore'),
	MessageStore = require('./MessageStore');

module.exports = {
    BaseContentStore: new BaseContentStore(),
    CategoryStore: new CategoryStore(),
    GlobalSettingsStore: new GlobalSettingsStore(),
    UserStore: new UserStore(),
    MessageStore: new MessageStore(),
    NewsStore: new NewsStore(),
    AlbumStore: new AlbumStore(),
    GalleryStore: new GalleryStore()
};