var _ = require('underscore');

var Fluxxor = require("fluxxor");

var ContentConstants = require('../constants/ContentConstants');

var CategoryStore = Fluxxor.createStore({
  initialize: function() {
    this.loading = false;
    this.adding = false;
    this.error = false;
    this.uploadingDocument = false;
    this.gallery = [];

    this.bindActions(
      ContentConstants.LOAD_CONTENT_CATEGORY, this.onLoad,
      ContentConstants.LOAD_CONTENT_CATEGORY_SUCCESS, this.onLoadSuccess,
      ContentConstants.LOAD_CONTENT_CATEGORY_FAIL, this.onLoadFail,

      ContentConstants.ADD_CATEGORY, this.onAdd,
      ContentConstants.ADD_CATEGORY_SUCCESS, this.onAddSuccess,
      ContentConstants.ADD_CATEGORY_FAIL, this.onAddFail,

      ContentConstants.EDIT_CATEGORY, this.onEdit,
      ContentConstants.EDIT_CATEGORY_SUCCESS, this.onEditSuccess,
      ContentConstants.EDIT_CATEGORY_FAIL, this.onEditFail,

      ContentConstants.REMOVE_CATEGORY, this.onRemove,
      ContentConstants.REMOVE_CATEGORY_SUCCESS, this.onRemoveSuccess,
      ContentConstants.REMOVE_CATEGORY_FAIL, this.onRemoveFail,

      ContentConstants.REMOVE_DOCUMENT, this.onDocumentRemove,
      ContentConstants.REMOVE_DOCUMENT_SUCCESS, this.onDocumentRemoveSuccess,
      ContentConstants.REMOVE_DOCUMENT_FAIL, this.onDocumentRemoveFail,

      ContentConstants.UPLOAD_DOCUMENT, this.onUploadDocument,
      ContentConstants.UPLOAD_DOCUMENT_SUCCESS, this.onUploadDocumentSuccess,
      ContentConstants.UPLOAD_DOCUMENT_FAIL, this.onUploadDocumentFail
    );
  },

  onLoad: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onLoadSuccess: function(data) {
    this.loading = false;
    this.categories = data;
    this.emit("change");
  },

  onLoadFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onAdd: function(data) {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onAddSuccess: function(data) {
    this.loading = false;
    this.categories.push(data);
    this.emit("change");
  },

  onAddFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onEdit: function(data) {
    this.loading = false;
    this.emit("change");
  },

  onEditSuccess: function(data) {
    this.loading = false;
    this.emit("change");
  },

  onEditFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onRemove: function(id) {
    this.loading = false;
    this.removingPending = id;
    this.emit("change");
  },

  onRemoveSuccess: function(data) {
    this.loading = false;
    this.categories = _.without(this.categories, _.findWhere(this.categories, {id: this.removingPending}));
    this.emit("change");
  },

  onRemoveFail: function() {
    this.loading = false;
    this.error = true;
    this.removingPending = null;
    this.emit("change");
  },

  onDocumentRemove: function(id) {
    this.loading = false;
    this.removingDocumentPending = id;
    this.emit("change");
  },

  onDocumentRemoveSuccess: function(data) {
    var self = this;
    var removedDocumentId = this.removingDocumentPending;
    this.loading = false;

    this.categories = _.map(this.categories, function (category) {
      var document = _.findWhere(category.documents, {id: removedDocumentId});
      if(document){
        category.documents = _.without(category.documents, document);

      }
      
      return category;
    });


    //_.without(this.categories, );
    this.emit("change");
  },

  onDocumentRemoveFail: function() {
    this.loading = false;
    this.error = true;
    this.removingPending = null;
    this.emit("change");
  },

  onUploadDocument: function () {
    this.uploadingDocument =true;
    this.error = null;
    this.emit("change");
  },

  onUploadDocumentSuccess: function (data) {
    this.uploadingDocument =false;
    var category = _.findWhere(this.categories, {id: data.document.category.id});

    category.documents.push(data.document);
    this.emit("change");
  },

  onUploadDocumentFail: function () {
    this.uploadingDocument =false;
    this.error = true;
    this.emit("change");
  },
  
  getState: function() {
    return {
      categories: this.categories
    };
  }
  
});

module.exports = CategoryStore;