var _ = require('underscore');

var Fluxxor = require("fluxxor");

var ContentConstants = require('../constants/ContentConstants');

var MessageStore = Fluxxor.createStore({
  initialize: function() {
    this.loading = false;
    this.error = false;

    this.bindActions(
	  ContentConstants.LOAD_CONTENT_MESSAGE, this.onLoad,
	  ContentConstants.LOAD_CONTENT_MESSAGE_SUCCESS, this.onLoadSuccess,
	  ContentConstants.LOAD_CONTENT_MESSAGE_FAIL, this.onLoadFail,

	  ContentConstants.REMOVE_MESSAGE, this.onRemove,
	  ContentConstants.REMOVE_MESSAGE_SUCCESS, this.onRemoveSuccess,
	  ContentConstants.REMOVE_MESSAGE_FAIL, this.onRemoveFail
	);
  },

  onLoad: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onLoadSuccess: function(data) {
    this.loading = false;
    this.messages = data;
    this.emit("change");
  },

  onLoadFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onRemove: function(id) {
    this.loading = false;
    this.removingPending = id;
    this.emit("change");
  },

  onRemoveSuccess: function(data) {
    this.loading = false;
    this.messages = _.without(this.messages, _.findWhere(this.messages, {id: this.removingPending}));
    this.emit("change");
  },

  onRemoveFail: function() {
    this.loading = false;
    this.error = true;
    this.removingPending = null;
    this.emit("change");
  },
});

module.exports = MessageStore;