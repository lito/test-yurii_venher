var Fluxxor = require("fluxxor");

var UserConstants = require('../constants/UserConstants');

var AuthClient = require('../utils/AuthClient');

var UserStore = Fluxxor.createStore({
  initialize: function() {
    this.loading = false;
    this.error = false;
    this.isAdmin = false;
    //this.userName = AuthClient.getUserName();
    this.authToken = AuthClient.getToken();
    this.loggedIn = AuthClient.loggedIn();
    this.isAdmin = AuthClient.isAdmin();

    this.bindActions(
      UserConstants.LOGIN_USER, this.onLogin,
      UserConstants.LOGIN_USER_SUCCESS, this.onLoginSuccess,
      UserConstants.LOGIN_USER_FAIL, this.onLoginFail,

      UserConstants.LOGOUT_USER, this.onLogout,
      UserConstants.LOGOUT_USER_SUCCESS, this.onLogoutSuccess,
      UserConstants.LOGOUT_USER_FAIL, this.onLogoutFail
    );
  },

  onLogin: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onLoginSuccess: function(userData) {
    this.loading = false;
    this.authToken = userData.token;
    //this.userName = userData.name;
    //this.userEmail = userData.userEmail;
    this.isAdmin = userData.isAdmin;
    this.loggedIn = true;
    this.emit("change");
  },

  onLoginFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onLogout: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onLogoutSuccess: function(token) {
    this.loading = false;
    this.authToken = null;
    this.loggedIn = false;
    this.isAdmin = false;
    this.emit("change");
  },

  onLogoutFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },
});

module.exports = UserStore;