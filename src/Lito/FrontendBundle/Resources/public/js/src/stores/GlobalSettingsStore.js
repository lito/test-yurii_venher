var Fluxxor = require("fluxxor");

var ContentConstants = require('../constants/ContentConstants');

var GlobalSettingsStore = Fluxxor.createStore({
  initialize: function() {
    this.loading = false;
    this.error = false;
    this.websiteTitle = null;
    this.rightColumnHTML = null;
    this.copyText = null;
    this.meta = null;
    this.contactName = null;
    this.address = null;
    this.contactPhone = null;
    this.contactEmail = null;
    this.logoId = null;
    this.logoCategoryName = null;
    this.logoDocumentName = null;

    this.bindActions(
      ContentConstants.LOAD_CONTENT_GLOBAL, this.onLoad,
      ContentConstants.LOAD_CONTENT_GLOBAL_SUCCESS, this.onLoadSuccess,
      ContentConstants.LOAD_CONTENT_GLOBAL_FAIL, this.onLoadFail,
      ContentConstants.EDIT_SETTINGS, this.onEdit,
      ContentConstants.EDIT_SETTINGS_SUCCESS, this.onEditSuccess,
      ContentConstants.EDIT_SETTINGS_FAIL, this.onEditFail
    );
  },

  setAttributes: function(obj) {
    this.websiteTitle = obj.title;
    this.copyText = obj.copy;
    this.meta = obj.meta; 
    this.contactName = obj.name;
    this.address = obj.address;
    this.contactPhone = obj.phone;
    this.contactEmail = obj.email;
    this.logoId = obj.logoId;
    this.logoCategoryName = obj.logoCategoryName;
    this.logoDocumentName = obj.logoDocumentName;
  },

  onLoad: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onLoadSuccess: function(objList) {
    var obj = objList[0];
    this.loading = false;
    this.setAttributes(obj);
    this.emit("change");
  },

  onLoadFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },

  onEdit: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onEditSuccess: function(obj) {
    this.loading = false;
    this.setAttributes(obj);
    this.emit("change");
  },

  onEditFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  }
});

module.exports = GlobalSettingsStore;