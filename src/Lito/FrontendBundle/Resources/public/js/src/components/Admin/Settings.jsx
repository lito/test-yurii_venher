'use strict';

var React = require("react"),
    Fluxxor = require("fluxxor"),
    FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;
 
var Router = require('react-router');
var { Route, RouteHandler, Link, Navigation } = Router;

var AdminMedia = require('./Media.jsx');
var Modal = require('./../Modal.jsx');
var ThumbnailItem = require('./../CategoryItem.jsx');


var EditBaseLogo = React.createClass({

  getInitialState: function() {
    return {
      openOverlay: null,
      logoId: null
    };
  },

  closeEdit: function () {
    this.props.cancelHandler();

  },

  openMedia: function () {
      this.setState({
        openOverlay: true
      });
  },

  handleOnOverlayClose: function () {
    this.setState({
        openOverlay: false
      });
  },

  handlePicker: function (document) {
    this.setState({
        openOverlay: false,
        logoId: document.id,
        logoDocumentName: document.name,
        logoCategoryName: document.categoryName
      });

    this.props.saveHandler({
      logoId: document.id
    });
  },

  render: function () {
    
    var openOverlay = this.state.openOverlay? 
      <Modal handleOnClose={this.handleOnOverlayClose} content={<AdminMedia handlePicker={this.handlePicker} />} limited={true} flux={flux} />:
      {};

    var logoId = (this.state.logoId || this.props.logoId); 
    var logoCategoryName = (this.state.logoCategoryName || this.props.logoCategoryName); 
    var logoDocumentName = (this.state.logoDocumentName || this.props.logoDocumentName); 
    //console.log("Tada: ", categoryName, documentName);

    return (
      <div>
        <div className="form-group clearfix">
          <div className="clearfix">
            <label>Logo</label>
          </div>
          <div>
            <ThumbnailItem category={logoCategoryName} name={logoDocumentName} />           
          </div>
          <div className="col-sm-5"> 
           <button type="submit" className="btn btn-default" onClick={this.openMedia}>Change logo</button>  
          </div>
           {openOverlay}
        </div>
      </div>
    );
  }
});

var AdminSettings = React.createClass({
      mixins: [FluxMixin, StoreWatchMixin("GlobalSettingsStore")],

      saveLogoElementIdToState: function (data) {
        this.setState({
          logoId: data.logoId
        });
        
      },

      getStateFromFlux: function() {
        var store = this.getFlux().store("GlobalSettingsStore");
        return {
          loading: store.loading,
          error: store.error,
          websiteTitle: store.websiteTitle,
          languages: store.languages,
          copyText: store.copyText,
          meta: store.meta,
          contactName: store.contactName,
          address: store.address,
          contactPhone: store.contactPhone,
          contactEmail: store.contactEmail,
          logoCategoryName: store.logoCategoryName,
          logoDocumentName: store.logoDocumentName,
          logoId: store.logoId
        };
      },

      updateItem: function (event) {
        event.preventDefault();
        var obj = {
          title: this.refs.websiteTitle.getDOMNode().value || this.state.websiteTitle,
          languages: this.state.languages,//this.refs.languages.getDOMNode().value || 
          copy: this.refs.copyText.getDOMNode().value || this.state.copyText,
          meta: this.state.meta, //this.refs.meta.getDOMNode().value || 
          name: this.refs.contactName.getDOMNode().value || this.state.contactName,
          address: this.refs.address.getDOMNode().value || this.state.address,
          phone: this.refs.contactPhone.getDOMNode().value || this.state.contactPhone,
          email: this.refs.contactEmail.getDOMNode().value || this.state.contactEmail,
          logoId: this.state.logoId
        }
        
        this.getFlux().actions.content.editSettings(obj);
      },

      render: function(){
          
          return (
              <div className="AdminSettings">
                <h2>AdminSettings</h2>
                <div className="form-group">
                  <label>Title</label>
                  <input type="text" className="form-control" placeholder={this.state.websiteTitle} ref="websiteTitle" />
                </div>
                <div className="form-group">
                  <label>Copyright Text</label>
                  <input type="text" className="form-control" placeholder={this.state.copyText} ref="copyText" />
                </div>
                <div className="form-group">
                  <label>Contact Name</label>
                  <input type="text" className="form-control" placeholder={this.state.contactName} ref="contactName" />
                </div>
                <div className="form-group">
                  <label>Address</label>
                  <input type="text" className="form-control" placeholder={this.state.address} ref="address" />
                </div>
                <div className="form-group">
                  <label>Contact Phone</label>
                  <input type="text" className="form-control" placeholder={this.state.contactPhone} ref="contactPhone" />
                </div>
                <div className="form-group">
                  <label>Contact Email</label>
                  <input type="text" className="form-control" placeholder={this.state.contactEmail} ref="contactEmail" />
                </div>
                <div className="logo-edit form-group">
                  <div className="clearfix">
                    <EditBaseLogo 
                      logoId={this.state.logoId}
                      logoDocumentName={this.state.logoDocumentName} 
                      logoCategoryName={this.state.logoCategoryName}
                      saveHandler={this.saveLogoElementIdToState} />
                    </div>
                </div>
                <button type="submit" className="btn btn-primary" onClick={this.updateItem}>Update</button>
            </div>
          );

      }

    });

module.exports = AdminSettings; 