/** @jsx React.DOM */

var React = require("react"),
    Fluxxor = require("fluxxor");

var FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;

 
var Router = require('react-router');
var { Route, RouteHandler, Link, Navigation } = Router;
  
var Header = require('./Header.jsx'),
    Footer = require('./Footer.jsx');


var Application = React.createClass({
    mixins: [Router.State, Navigation, FluxMixin, StoreWatchMixin("GlobalSettingsStore", "UserStore")],

    getDefaultProps: function () {
      return {
        flux: flux
      }
    },

    getInitialState: function() {
        return {
        }
    }, 

    componentDidMount: function() {
      var userAuthToken = (this.state.userAuthToken); 
      this.props.flux.actions.content.loadGalleryContent();
      this.props.flux.actions.content.loadGlobalSettings(); 
      this.props.flux.actions.content.loadCategories();
    },

    getStateFromFlux: function() {
      var globalSettingsStore = this.props.flux.store("GlobalSettingsStore");
      var userStore = this.props.flux.store("UserStore");
      return {
        loading: globalSettingsStore.loading,
        error: globalSettingsStore.error,
        websiteTitle: globalSettingsStore.websiteTitle,
        copyText: globalSettingsStore.copyText,
        address: globalSettingsStore.address,
        contactName: globalSettingsStore.contactName,
        contactPhone: globalSettingsStore.contactPhone,
        contactEmail: globalSettingsStore.contactEmail,
        userLoggedIn: userStore.loggedIn,
        userName: userStore.userName,
        userAuthToken: userStore.authToken,
        userIsAdmin: userStore.isAdmin,
        logoId: globalSettingsStore,
        logoCategoryName: globalSettingsStore.logoCategoryName,
        logoDocumentName: globalSettingsStore.logoDocumentName
      };
    },

    getHandlerKey: function () {
        // this will all depend on your needs, but here's a typical
        // scenario that's pretty much what the old prop did
        var childDepth = 1; // have to know your depth
        var childName = this.getRoutes()[childDepth].name;
        var id = this.getParams().id;
        var key = childName+id;
        return key;
      },

    render: function() {
      var copyText = this.state.copyText? this.state.copyText : null;
      var address = this.state.address? this.state.address : null;
      var contactName = this.state.contactName? this.state.contactName :null;
      var contactPhone = this.state.contactPhone? this.state.contactPhone :null;
      var contactEmail = this.state.contactEmail? this.state.contactEmail :null;
      var loggedIn = this.state.userLoggedIn? true : false;
      var isAdmin = this.state.userIsAdmin? true : false;
      var currentPage = this.getRoutes()[1].name || "start";
      
      if(this.state.websiteTitle) {
        this.setTitle(this.state.websiteTitle); 
      }
      return (
        <div id="container" className="container" data-current-page={currentPage}>
          <Header loggedIn={loggedIn} isAdmin={isAdmin} logoCategoryName={this.state.logoCategoryName} logoDocumentName={this.state.logoDocumentName} />
          <RouteHandler key={this.getHandlerKey()} isAdmin={isAdmin} />
          <Footer copyText={copyText} address={address} contactName={contactName} contactPhone={contactPhone} contactEmail={contactEmail} loggedIn={loggedIn} isAdmin={isAdmin} />
        </div>
      );
  },

  setTitle: function (text) {
    document.title = text;
  
  }
});

module.exports = Application;