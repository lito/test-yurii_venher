/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery');
var _ = require('underscore');
var React = require('react');

var AddItem = React.createClass({

  render: function () {
    return (
      <div>
      	<button type="submit" className="btn btn-primary" onClick={this.props.editHandler}>Add</button>
      </div>
    );
  }
});

module.exports = AddItem;