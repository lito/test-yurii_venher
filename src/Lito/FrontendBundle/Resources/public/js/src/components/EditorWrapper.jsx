/**
 * @jsx React.DOM
 */

'use strict';
var React = require('react');

var _ = require('underscore');

var Editor = require('ainojs-reacti-editor'),
    EditorStyle = require('ainojs-reacti-editor-css');


var AdminMedia = require('./Admin/Media.jsx');
var Modal = require('./Modal.jsx');



var EditorWrapper = React.createClass({

  getInitialState() {
  	return {
  		content: this.props.content
  	};

  },	

  getDefaultProps: function () {
    return {
        basePath: '/uploads/',
      };
  },

  handleOnOverlayClose: function () {
    this.setState({
      openOverlayForEditor: false
    });
  },

  openMediaForEditor: function () {
     this.setState({
       openOverlayForEditor: true
     });
   },

  handlePickerForEditor: function (document) {
      var path = this.props.basePath + document.categoryName + '/' + document.name;
      this.refs.editor.refs.injectinput.getDOMNode().value = path;
      var a = this.refs.editor.inject('image');
      a();

      this.handleOnOverlayClose();	
  },

  render: function () {
    var openOverlay;
   
    if(this.state.openOverlayForEditor) {
      openOverlay = <Modal handleOnClose={this.handleOnOverlayClose} content={<AdminMedia handlePicker={this.handlePickerForEditor} />} />;  
    }

    return (
      	<div className="editor-wrapper">
	      <Editor onChange={this.props.onChange} html={this.state.content} openModal={this.openMediaForEditor} ref="editor" />
	      {openOverlay}
	    </div>
    );
  }
});


module.exports = EditorWrapper;