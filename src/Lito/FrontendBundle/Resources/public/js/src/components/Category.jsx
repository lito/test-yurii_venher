  /**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery');
var _ = require('underscore');
var React = require('react');
var Router = require('react-router');
var Fluxxor = require("fluxxor"),
    FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;

var DefaultLayout = require('../layouts/DefaultLayout.jsx');

var CategoryItem = require('./CategoryItem.jsx');

var CategoryComponent = React.createClass({

    getDefaultProps: function () {
      return {
        basePath: '/uploads/'
      };
    },

    render: function() {
      var categoryName = this.props.name;
      
      var categoryList = _.map(this.props.currentCategory.documents, function(obj, i) {
        return <CategoryItem key={i} category={categoryName} name={obj.name} title={obj.name} />;
      });

      return (
      	<div className="category-page">
	      	<div className="category-list">
	      		{categoryList}
	      	</div>
	      	<div id="blueimp-gallery" className="blueimp-gallery blueimp-gallery-controls" data-use-bootstrap-modal="false">
         
            <div className="slides"></div>
          
            <h3 className="title"></h3>
            <a className="prev">‹</a>
            <a className="next">›</a>
            <a className="close">×</a>
            <a className="play-pause"></a>
            <ol className="indicator"></ol>
            
            <div className="modal fade">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" aria-hidden="true">&times;</button>
                            <h4 className="modal-title"></h4>
                        </div>
                        <div className="modal-body next"></div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default pull-left prev">
                                <i className="glyphicon glyphicon-chevron-left"></i>
                                Previous
                            </button>
                            <button type="button" className="btn btn-primary next">
                                Next
                                <i className="glyphicon glyphicon-chevron-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
          </div>
      	</div>
      	

      	
      	);
    }

});

module.exports = CategoryComponent;