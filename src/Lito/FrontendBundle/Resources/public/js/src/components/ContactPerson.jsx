/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery');
window.jQuery = require('jquery');
var React = require('react'),
    Editor = require('ainojs-reacti-editor'),
    EditorStyle = require('ainojs-reacti-editor-css'),
    Fluxxor = require("fluxxor"),
    FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;

var AdminMedia = require('./Admin/Media.jsx');
var ImageItem = require('./CategoryItem.jsx');
var Modal = require('./Modal.jsx');

var EditBaseContent = React.createClass({

  getInitialState: function() {
    return {
      openOverlay: null,
      documentId: null,
      body: this.props.body
    };
  },

  updateElement: function () {
    this.props.saveHandler({
      title: (this.refs.title.getDOMNode().value || this.props.title),
      body: this.state.body,
      documentId: (this.state.documentId || this.props.documentId),
      link: "#",
      identifiedWith: "contactPerson"
    });
  },
  
  closeEdit: function () {
    this.props.cancelHandler();

  },

  openMedia: function () {
      this.setState({
        openOverlay: true
      });
  },

  handleOnOverlayClose: function () {
    this.setState({
        openOverlay: false
      });
  },

  handlePicker: function (document) {
    this.setState({
        openOverlay: false,
        documentId: document.id,
        documentName: document.name,
        categoryName: document.categoryName
      });
  },

  onBodyChange: function (value) {
    this.setState({
      body: value
    });
  },

  render: function () {

    var openOverlay = this.state.openOverlay? 
      <Modal handleOnClose={this.handleOnOverlayClose} content={<AdminMedia handlePicker={this.handlePicker} />} flux={flux} />:
      {};

    var documentId = (this.state.documentId || this.props.documentId); 
    var categoryName = (this.state.categoryName || this.props.categoryName); 
    var documentName = (this.state.documentName || this.props.documentName);

    return (
      <div className="list-group-item">
        <div className="form-group clearfix">
          <input type="text" className="form-control" placeholder="Title" defaultValue={this.props.title} ref="title" />
        </div>
        <div className="form-group clearfix">
          <div className="col-sm-6">
            <ImageItem category={categoryName} name={documentName} />           
          </div>
          <div className="col-sm-6"> 
           <button type="submit" className="btn btn-default" onClick={this.openMedia}>New Image</button>  
          </div>
           {openOverlay}
        </div>
        <div className="form-group clearfix">
          <label>Contact Info</label>
          <div className="editor-wrapper">
            <Editor onChange={this.onBodyChange} html={this.state.body} ref="body" />
          </div>
        </div>
        <div className="form-group clearfix">
          <div className="btn-group">
            <button type="submit" className="btn btn-primary" onClick={this.updateElement}>Save</button>
            <button type="submit" className="btn btn-default" onClick={this.closeEdit}>Cancel</button>
          </div>
        </div>
      </div>
    );
  }
});

var ReadBaseContent = React.createClass({
  render: function () {
  var admin = this.props.isAdmin?
    <div className="form-group clearfix">
      <button type="submit" className="btn btn-primary" onClick={this.props.editHandler}>Edit</button>
    </div>:
    {};

  return (
    <div className="clearfix">
      <div className="clearfix">
        <h2>{this.props.title}</h2>
      </div>
      <div className="list-group-item">
        <div className="clearfix">
          <div className="contact-person-img col-sm-4">
            <ImageItem category={this.props.categoryName} name={this.props.documentName} />
          </div>
          <div className="col-sm-8">
            <div dangerouslySetInnerHTML={{__html: this.props.body}}></div>
          </div>
          {admin}
        </div>
        </div>
    </div>
  );
  }
});

var ContactPersonSection = React.createClass({

  mixins: [FluxMixin],

  getInitialState: function() {
    return {
      isReadMode: true
    };
  },

  saveElement: function (data) {
    this.getFlux().actions.content.editBaseContent(this.props.content.id, data, 'contactPerson');

    this.activateReadMode();
  },

  activateReadMode: function () {
    this.setState({
      isReadMode: true
    });
  },

  editElement: function () {
    this.setState({
      isReadMode: false
    });
  },

  render: function () {

    var content = this.props.content;
    var mode = (this.props.isAdmin && !this.state.isReadMode)?

    <EditBaseContent 
      saveHandler={this.saveElement} 
      cancelHandler={this.activateReadMode}
      title={content.title} 
      body={content.body}
      documentId={content.documentId}
      documentName={content.documentName}
      categoryName={content.documentCategoryName}
      items={content} />:

    <ReadBaseContent 
      isAdmin={this.props.isAdmin} 
      editHandler={this.editElement} 
      title={content.title} 
      body={content.body}
      documentId={content.documentId}
      documentName={content.documentName}
      categoryName={content.documentCategoryName}
      items={content} />;

    return (
      <div>
        {mode}
      </div>
    );
    }
});

var ContactPerson = React.createClass({
  mixins: [Router.State, FluxMixin, StoreWatchMixin("BaseContentStore")],

  getDefaultProps: function () {
    return {
      title: "ContactPerson",
    };
  },
  
  getStateFromFlux: function() {
    var baseContentStore = this.getFlux().store("BaseContentStore");
    return{
      contactPerson: (baseContentStore.contentList['contactPerson'] ? baseContentStore.contentList['contactPerson'][0] : null),
    }
  },

  componentDidMount: function () {

    if(!this.state.contentList) {
      this.getFlux().actions.content.loadBaseContent('contactPerson');
    }
  },

 
  render: function () {
    var contactPerson = this.state.contactPerson?
        <ContactPersonSection content={this.state.contactPerson} isAdmin={this.props.isAdmin} />:
      {};
    return (
      <div className="baseLayout clearfix">
        {contactPerson}
      </div>
    );
  }
});

module.exports = ContactPerson;