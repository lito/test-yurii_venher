/**
 * @jsx React.DOM
 */

'use strict';
var React = require('react');

var $ = require('jquery');
var _ = require('underscore');

var ContentItem = require('./../mixins/ContentItem.js');


var ContentList = require('./BaseContentList.jsx');

var Fluxxor = require("fluxxor"),
    FluxMixin = Fluxxor.FluxMixin(React);

var AdminMedia = require('./Admin/Media.jsx');
var ImageItem = require('./CategoryItem.jsx');
var AddContentItem = require('./AddContentItem.jsx');
var Modal = require('./Modal.jsx');

var ReadAlbumItem = React.createClass({

  mixins: [ContentItem.ReadMode],

  render: function () {

    var adminButtons = this.renderButtons();
    
    return (
      <div className="album-item col-xs-3">
        <ImageItem 
          category={this.props.obj.categoryName} 
          name={this.props.obj.documentName}
          title={this.props.obj.title} />

        {adminButtons}

      </div>
    );
  }
});


var WriteAlbumItem = React.createClass({

  getInitialState: function() {
    return {
      title: this.props.obj && this.props.obj.title || null,
    };
  },

  mixins: [ContentItem.WriteMode],

  addItem: function (event) {
    this.props.onAdd({
      title: this.refs.title.getDOMNode().value,
      documentId: this.state.imageId || this.state.documentId,
      orderId: 1,                                   
      documentName: this.state.documentName,
      categoryName: this.state.categoryName
    });
    
    this.props.cancelHandler();
  },

  updateItem: function (event) {

    var obj = this.props.obj;
    obj.title = this.refs.title.getDOMNode().value || obj.title;
    obj.documentId = this.state.imageId || this.state.documentId;
    obj.documentName = this.state.documentName,
    obj.categoryName = this.state.categoryName;
    obj.orderId = this.props.obj.orderId;

    this.props.onEdit(obj);
    this.props.cancelHandler();
  },


  render: function () {

    var title = this.state.title, 
        documentName = (this.state.documentName || this.props.obj && this.props.obj.documentName),
        categoryName = (this.state.categoryName || this.props.obj && this.props.obj.documentCategoryName),
        buttons = this.renderButtons(),
        openOverlay = this.renderOverlay(Modal, AdminMedia);

    return (
      <div className="album-item-edit col-xs-3"> 
        <div className="album-item-content form-group col-xs-12">
          <div className="row">
            <div className="album-item-title form-group col-xs-12">
              <input type="text" className="form-control" placeholder="Image title" defaultValue={title} ref="title" />
            </div>
          </div>
          <div className="album-item-image form-group col-xs-12">
            {documentName ? <ImageItem 
              category={categoryName} 
              name={documentName}
              title={title} /> : {}} 
          </div>
          
          <div className="row>">
            <div className="change-image-button form-group col-sm-12"> 
              <button type="submit" className="btn btn-default" onClick={this.openMedia}>Change image</button>  
                {openOverlay}
            </div>
          </div>
          <div className="row">
            <div className="edit-buttons form-group col-sm-12">
              <div className="btn-group"> 
                {buttons}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

var AlbumItem = React.createClass({
  
  mixins: [ContentItem.BaseContent],

  render: function () {

    return (
      <div  onDragStart={this.dragItem} 
            onDragEnter={this.enterItem} 
            onDragOver={this.overItem} 
            onDrop={this.dropOnItem} >

        {this.renderMode(ReadAlbumItem, WriteAlbumItem, AddContentItem)}

      </div>
    );
  }
});



var WriteAlbum = React.createClass({
 
  getInitialState() {
    return {
      items: (this.props.obj && this.props.obj.items/*.sort(function(a,b) {return a.orderId - b.orderId}) */|| [])
    };
  },

  mixins: [ContentItem.WriteMode],

  addItem: function (album) {
    event.preventDefault();
    this.props.onAdd({
      title: this.refs.title.getDOMNode().value,
      intro: this.refs.intro.getDOMNode().value,
      link: this.refs.link.getDOMNode().value,
      items: this.state.items,
      body: ""
    });
    
    this.props.cancelHandler();
  },

  updateItem: function (event) {

    event.preventDefault();
    var obj = this.props.obj;
    obj.title = this.refs.title.getDOMNode().value;
    obj.intro = this.refs.intro.getDOMNode().value;
    obj.link = this.refs.link.getDOMNode().value;
    obj.body = obj.body || "";
    obj.items = this.state.items;
    this.props.onEdit(this.props.obj.id, obj);
    this.props.cancelHandler();
  },

  onAddItem (obj) {
    this.state.items.push(obj);
    
    this.setState({
      items: this.state.items
    });
  },

  onEditItem (obj) {

    var items = _.map(this.state.items, function (item) {
      if(item.id === obj.id){
        item = obj;
      }
      return item;
    });

    this.setState({
      items: items
    });
  },

  onRemoveItem (id) { 
    this.setState({
      items: _.without(this.state.items, _.findWhere(this.state.items, {documentId: id}))
    });
  },
  
  onSwapItems (transferData) {

    var items = _.map(this.state.items, function(item) {
      if (item.id === transferData.sourceId) {
        item.orderId = transferData.targetOrder;
      }
      if (item.id === transferData.targetId) {
        item.orderId = transferData.sourceOrder;
      }
      return item;
    });

    this.setState({
      items: items.sort(function(a,b) { return a.orderId - b.orderId; })
    });
  },

  render: function () {
    var title, intro, items = this.state.items, link, albumItems;
    var buttons = this.renderButtons();

    if(!this.props.isAddMode) {
      title = this.props.obj.title,
      intro = this.props.obj.intro;
      link = this.props.obj.link;
    }

    albumItems = 
      <ContentList
        list={_.map(items, function (item) {
          item.categoryName = item.documentCategoryName || item.categoryName;
          item.documentId = item.documentId || item.id;
          item.title = item.title || "";
          item.orderId = item.orderId || 0;
          return item;
        })}
        isAdmin={true} 
        onAdd={this.onAddItem}
        onEdit={this.onEditItem}        
        onRemove={this.onRemoveItem}
        onSwap={this.onSwapItems}
        element={AlbumItem} />;

    return (
      <div className="list-group-item row">
        <div className="album-content-edit form-group col-xs-12">
          <div className="row">
            <div className="album-title form-group col-xs-12">
              <input type="text" className="form-control" placeholder="Title" defaultValue={title} ref="title" />
            </div>
          </div>
          <div className="row">
            <div className="album-intro form-group col-xs-12">
              <input type="text" className="form-control" placeholder="Intro" defaultValue={intro} ref="intro" />
            </div>
          </div>
          <div className="row">
            <div className="album-link form-group col-xs-12">
              <input type="text" className="form-control" placeholder="Link" defaultValue={link} ref="link" />
            </div>
          </div>
          <div className="row">
            <div  className="album-images-list form-group col-xs-12">
              {albumItems}
            </div>
          </div>
          
          <div className="row">
            <div className="edit-buttons form-group col-sm-12">
              <div className="btn-group"> 
                {buttons}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});


var ReadAlbum = React.createClass({

  mixins: [ContentItem.ReadMode],

  renderAlbumItems: function () {
    var itemOrder = 1;
    var albumItems = 
      <ContentList 
        list={_.map(this.props.obj.items, function (item) {
          item.categoryName = item.documentCategoryName || item.categoryName;
          item.title = item.title || "";
          item.orderId = itemOrder++;
          return item;
        })}  
        isAdmin={false}
        element={AlbumItem} />;
   
    return (
      <div>
        {albumItems}
      </div>
    );
  },
  
  render: function () {

    var intro, items, 
    adminButtons = this.renderButtons();
    
    if(this.props.obj.items.length) {
      items = this.renderAlbumItems();
    }

    return (
      <div className="clearfix">
        <div className="row">
          <div className="album-content form-group col-xs-12">
            <div className="row">
              <h3>{this.props.obj.title}</h3>
            </div>
            <div className="row">
              <div className="album-intro col-xs-10">
                {this.props.obj.intro}
              </div>
              <div className="col-xs-2">
                <a href={this.props.obj.link} className="btn btn-lg btn-warning">Visit online-shop</a>
              </div>
            </div>
            <div className="album-images-list list-group-item row">
              {items}
            </div>
          </div>
        </div>
        {adminButtons}
      </div>
    );
  }
});

var AlbumContent = React.createClass({

  mixins: [ContentItem.BaseContent],

  render: function () {
  
    return (
      <div className="albums-item">{this.renderMode(ReadAlbum, WriteAlbum, AddContentItem)}</div>
    );
  }
});


module.exports = AlbumContent;