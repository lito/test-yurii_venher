/** @jsx React.DOM */

var React = require("react");
var Router = require('react-router'),
    Navigation = Router.Navigation,
    Link = Router.Link;

var Footer = React.createClass({

      render: function () {
              var loginOrOut = this.props.loggedIn ?
              <Link to="logout">Log out</Link> :
              null;
              
              var admin = (this.props.loggedIn && this.props.isAdmin)? 
              <Link to="settings">Admin</Link>:
              null;

              return (
                  <footer className="footer container clearfix center-block bs-docs-footer-links muted">
                      <div className="navbar-footer">
                        <div className="adm">{admin}{loginOrOut}</div>
                        <div className="legal-text-link">
                          <Link to="impressum">Impressum</Link>
                          <Link to="datenschutz">Datenschutz</Link>
                        </div>
                        <h5 className="copy">{this.props.copyText}</h5>
                        <h5 className="address">{this.props.address}  {this.props.contactPhone} {this.props.contactEmail}</h5>
                        <h5 className="contact-name">{this.props.contactName}</h5>
                        <div className="social-block">
                          <div className="social-block-icon">
                            <a id="facebook-icon" href="https://www.facebook.com/AtelierTatjanaPrijmak" target="_blank"></a>
                          </div>
                          <div className="social-block-icon">
                            <a id="twitter-icon" href="https://twitter.com/" target="_blank"></a>
                          </div>
                          <div className="social-block-icon">
                            <a id="plus-google-icon" href="http://www.google.com" target="_blank"></a>
                          </div>
                          <div className="social-block-icon">
                            <a id="skype-icon" href="http://www.skype.com" target="_blank"></a>
                          </div>
                        </div>
                      </div>          
                  </footer>
              );
      }
});

module.exports = Footer
