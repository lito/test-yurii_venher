/** @jsx React.DOM */

var React = require("react"),
    Fluxxor = require("fluxxor");

var FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;

var Router = require('react-router'),
    Navigation = Router.Navigation;

var Login = React.createClass({
  statics: {
    attemptedTransition: null
  },

  mixins: [Navigation, FluxMixin, StoreWatchMixin("UserStore")],

  getStateFromFlux: function() {
    var store = this.getFlux().store("UserStore");
    //this.transitionTo('start'); 
    return {
      loading: store.loading,
      error: store.error,
      loggedIn: store.loggedIn,
      authToken: store.authToken,
      isAdmin: store.isAdmin
    };
  },

  handleSubmit: function(event) {
    event.preventDefault();
    var email = this.refs.email.getDOMNode().value;
    var pass = this.refs.pass.getDOMNode().value;
    this.getFlux().actions.user.loginUser(email, pass); 
  },

  redirectAfterLogin: function () {
      
      if(this.state.isAdmin) {
        this.replaceWith('settings');    
      }
  },

  componentDidUpdate: function (nextProps, nextState) {
    
    if(this.state.loggedIn) {
      this.redirectAfterLogin();
    }

  },

  render: function() {
    var errors = this.state.error ? <p>Bad login information</p> : '';

    
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label><input ref="email" placeholder="login"/></label>
          <label><input ref="pass" placeholder="password"/></label><br/>
          <button type="submit">login</button>
          {errors}
        </form>
      </div>
      
    );
  }
});

module.exports = Login;