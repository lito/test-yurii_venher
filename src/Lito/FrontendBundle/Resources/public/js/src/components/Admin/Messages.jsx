var React = require("react"),
    Fluxxor = require("fluxxor");

var FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;

 
var Router = require('react-router');
var { Route, RouteHandler, Link, Navigation } = Router;

var MessageItem = React.createClass({
	mixins: [FluxMixin],

	removeItem: function (event) {
	  event.preventDefault();
	  
	  this.getFlux().actions.content.removeMessage(this.props.id);
	},

	render: function(){
		var name = this.props.message.name,
		    from = this.props.message.email,
		    date = new Date(this.props.message.date),
		    text = this.props.message.message;

		var utcDate = date.toUTCString();

		return (
		  <li className="list-group-item form-inline row">
		  	<div className="row">
			  	<div className="form-group col-md-4">
			  	  <p className="form-control-static" id="message-date" name="message-date">{utcDate}</p>
			  	</div>
		  		<div className="form-group col-md-8"></div>
		  	</div>
		  	<div className="row">
			    <div className="form-group col-xs-2">
			      <p className="form-control-static" name="message-name">{name}</p>
			    </div>
			    <div className="form-group col-xs-3">
			      <p className="form-control-static" name="message-email">{from}</p>
			    </div>
			    <div className="form-group col-md-5">
			      <p className="form-control-static" name="message-message">{text}</p>
			    </div>
			    <div className="form-group col-xs-2">
			      <div className="btn-group"> 
			        <button type="submit" className="btn btn-default" onClick={this.removeItem}>Remove</button>
			      </div>
			    </div>
			  </div>
		  </li>
	  )
	}
});

var MessageList = React.createClass({
  render: function(){
    var list = _.map(this.props.messages, function(obj) {
        return <MessageItem message={obj} id={obj.id} />
    });

    return (
        <div id="message-list" className="message-list clearfix">
            <h4>List of inboxing messages</h4>
            <ul className="list-group">
              {list}
            </ul>
        </div>     
    )
  }
});

var AdminMessages = React.createClass({
  mixins: [FluxMixin, StoreWatchMixin("MessageStore")],

  getStateFromFlux: function() {
    var store = this.getFlux().store("MessageStore");
    return {
      loading: store.loading,
      error: store.error,
      messages: store.messages
    };
  },

  componentDidMount: function () {
  	if(!this.state.messages) {
  		this.getFlux().actions.content.loadMessages();
  	}
  },

  render: function(){
      
      return (
          <div className="AdminMessages">
              <h2>Messages</h2>
							<MessageList messages={this.state.messages}/>
          </div>
      );

  }
});

module.exports = AdminMessages;

