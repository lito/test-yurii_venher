/** @jsx React.DOM */

var $ = require('jquery');
window.jQuery = require('jquery');

var React = require("react"),
    modal = require('bootstrap/js/modal');

var Modal = React.createClass({

  componentDidMount: function () {
      var self = this;
      $(".section").removeClass("active");
      $('#modal-wrapper').modal('show');

      $('.modal-dialog').height($( window ).height() - 100);

      if(self.props.handleOnClose) {
        $('#modal-wrapper').on('hidden.bs.modal', function (e) {
          self.props.handleOnClose();
        });  
      }
      
  },
  
  render: function () {
    return (
      <div className="modal fade" id="modal-wrapper" tabindex="-1" role="dialog">
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 className="modal-title">Edit</h4>
            </div>
            <div className="modal-body">
              {this.props.content}
            </div>
          </div>
        </div>
      </div>
    );
  }

});

module.exports = Modal;