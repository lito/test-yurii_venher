var AuthenticatedRoute = {
  statics: {
    willTransitionTo: function (transition) {
      if (!window.flux.store("UserStore").isAdmin) {
        transition.redirect('/login');
      }
    }
  }
};

module.exports = AuthenticatedRoute;
