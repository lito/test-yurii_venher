/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery');
var React = require('react');
var Fluxxor = require("fluxxor"),
    FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;

var ContactPage = React.createClass({
  mixins: [FluxMixin, StoreWatchMixin("GlobalSettingsStore")],

  getStateFromFlux: function() {
    var store = this.getFlux().store("GlobalSettingsStore");
    return {
      loading: store.loading,
      error: store.error,
      contactName: store.contactName,
      address: store.address,
      contactPhone: store.contactPhone,
      contactEmail: store.contactEmail
    };
  },

  getDefaultProps: function () {
    return {
      title: "Contact",
    };
  },
  getInitialState: function() {
    return {
      sendStatus: null,
      formLoading: false
    };
  },

  submitForm: function (event) {
    var self = this,
        form = $('#contact-form'); 

    event.preventDefault();
    if(this.state.formLoading) return;
    
    
    this.setState({
      formLoading: true
    });
    
    
    // TBD 
    // add validation 
    // dynamic URI
    $.ajax({
        dataType: "json",
        contentType: "application/json", 
        type: "POST",
        url: '/api/contact/form',
        success: function(data) {
          self.setState({
            sendStatus: "Thank you. The message has been sent successfully",
            formLoading: false
          });

        },
        error: function(data) {
          self.setState({
            formLoading: false
          });

        },
        data: JSON.stringify({
          name: this.refs.name.getDOMNode().value,
          email: this.refs.email.getDOMNode().value,
          textarea: this.refs.message.getDOMNode().value
        })
    });
  },

  modifyForm: function () {
    this.setState({
      sendStatus: null
    });

  },
  
  render: function() {
    var btnText = this.state.formLoading? "Loading ..." : "Send";
    
    return (
  		<div className="cms clearfix">
        <div className="row">
          <div className="col-sm-10">
          <h1>{this.props.title}</h1>
          </div>
          <div className="col-sm-5">
            
            <form id="contact-form" className="well" method="POST" enctype="multipart/form-data" onSubmit={this.submitForm} onChange={this.modifyForm} >
              <div className="form-group">
                <label for="name">Name</label>
                <input type="name" className="form-control" id="name" placeholder="Name" ref="name" />
              </div>
              <div className="form-group">
                <label for="email">Email address</label>
                <input type="email" className="form-control" id="email" placeholder="Enter email" ref="email" />
              </div>
              <div className="form-group">
                <label for="message">Message</label>
                <textarea className="form-control" rows="5" id="message" ref="message" ></textarea>
              </div>
              <button type="submit" className="btn btn-primary">{btnText}</button>
              <div>{this.state.sendStatus}</div>
            </form>
          </div>
          <div className="col-sm-5">
            <ul className="list-unstyled">
              <li><b>{this.state.contactName}</b></li>
              <li>{this.state.address}</li>
              <li>{this.state.contactPhone}</li>
              <li>{this.state.contactEmail}</li>
            </ul>
          </div>
        </div>  
        
        
      </div>
  	);
  }
});

module.exports = ContactPage;
