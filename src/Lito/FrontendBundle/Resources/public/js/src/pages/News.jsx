/**
 * @jsx React.DOM
 */

'use strict';

var $ = require('jquery'),
	React = require('react'),
	Fluxxor = require("fluxxor"),
	FluxMixin = Fluxxor.FluxMixin(React),
	StoreWatchMixin = Fluxxor.StoreWatchMixin,
	Router = require('react-router'),
	Navigation = Router.Navigation;

var ContentList = require('../components/BaseContentList.jsx'),
	EventContent = require('../components/EventContent.jsx'),
	ContactPerson = require('../components/ContactPerson.jsx');


var NewsPage = React.createClass({

	mixins: [FluxMixin, StoreWatchMixin("NewsStore")],

	getInitialState: function() {
		return {
			isContentLoaded: false
		};
	},
	
	getDefaultProps: function () {
		return {
			title: "News",
			isContentLoaded: false
		};
	},
	
	getStateFromFlux: function() {
		var newsAndEventsStore = this.getFlux().store("NewsStore");
		
		return {
			 news: newsAndEventsStore.news,
			 events: newsAndEventsStore.events
		};
	},

	componentDidMount: function () {
		 this.getFlux().actions.content.loadNews();
		 this.getFlux().actions.content.loadEvents();
	},
 
	render: function () {
		var newsList, eventList;

		if(this.state.news) {
			newsList = 
			<ContentList 
				list={this.state.news} 
				isAdmin={this.props.isAdmin} 
				onAdd={this.getFlux().actions.content.addNews}
				onEdit={this.getFlux().actions.content.editNews}
				onRemove={this.getFlux().actions.content.removeNews} />;
		}

		if(this.state.events) {
			eventList = 
			<ContentList 
				list={this.state.events}  
				isAdmin={this.props.isAdmin} 
				onAdd={this.getFlux().actions.content.addEvent}
				onEdit={this.getFlux().actions.content.editEvent}
				onRemove={this.getFlux().actions.content.removeEvent}
				element={EventContent}  />;
		}

		
		return (
			<div className="baseLayout clearfix">
				<div className="centered">
					<div className="row">
						<div className="news-block col-sm-8">
							<h2 className="page-title">{this.props.title}</h2>
							<div className="list-group-item row">
								{newsList}
							</div>
						</div>
						<div className="events-block col-sm-4">
							<h2 className="page-title">Events</h2>
							<div className="list-group-item row">
								{eventList}
							</div>
							<div className="contact-person-section row">
								<ContactPerson isAdmin={this.props.isAdmin} />
							</div>
						</div>
					</div>
				</div>
				
			</div>
		);
	}
});

module.exports = NewsPage;