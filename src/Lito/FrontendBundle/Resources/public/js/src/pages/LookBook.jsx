/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery');
window.jQuery = require('jquery');
var React = require('react');
var Fluxxor = require("fluxxor"),
    FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin,
    Modal = require('bootstrap/js/modal');

var ContentList = require('../components/BaseContentList.jsx'),
	AlbumContent = require('../components/AlbumContent.jsx');    


var LookBookPage = React.createClass({

	mixins: [Router.State, FluxMixin, StoreWatchMixin("AlbumStore")],

	getInitialState: function() {
		return {
			isContentLoaded: false
		};
	},

	getDefaultProps: function () {
		return {
			title: "Lookbook"
		};
	},

	getStateFromFlux: function() {
		var albumStore = this.getFlux().store("AlbumStore");
		return {
			 albums: albumStore.albums
		};
	},

	componentDidMount: function () {
		this.getFlux().actions.content.loadAlbums();
	},

	render: function () {
		var albumsList;

		if(this.state.albums) {
			albumsList = 
			<ContentList 
				list={this.state.albums}  
				isAdmin={this.props.isAdmin} 
				onAdd={this.getFlux().actions.content.addAlbum}
				onEdit={this.getFlux().actions.content.editAlbum}
				onRemove={this.getFlux().actions.content.removeAlbum}
				element={AlbumContent} />;
		}

		return (
			<div className="baseLayout clearfix">
				<div className="centered">
					<div className="row">
						<div className="lookbook-content col-sm-12">
							<h2 className="page-title">{this.props.title}</h2>
							{albumsList}
						</div>
					</div>
				</div>
				
			</div>
		);
	}
});

module.exports = LookBookPage;