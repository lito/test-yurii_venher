/**
 * @jsx React.DOM
 */

'use strict';


var React = require('react');
var DefaultLayout = require('../layouts/DefaultLayout.jsx');
var Router = require('react-router');
var Login = require('../components/Login.jsx');
var Logout = require('../components/Logout.jsx');

var LoginPage = React.createClass({
  mixins: [ Router.State ],
  render: function () {

    var LogInOrOut = _.findWhere(this.context.getCurrentRoutes(), {name:'login'}) ?
          <Login /> :
          <Logout />;
    return (
      <div className="pageContent container">
        <div className="row">
          <div className="col-sm-4">
            
          </div>
          <div className="col-sm-4">
            <h3>Login</h3>
            {LogInOrOut}
          </div>
          <div className="col-sm-4">
            
          </div>
        </div>
      </div>
    );
  }
});

module.exports = LoginPage;
