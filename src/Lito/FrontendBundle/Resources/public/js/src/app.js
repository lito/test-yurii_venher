/** @jsx React.DOM */
// globals
window._ = require('underscore');
window.React = require("react");
window.Router = require('react-router');
window._GLOBALS.featureFlag = require('./utils/FeatureFlag');
// bootstrap
var routes = require('./router.jsx');

Router.run(routes, Router.HistoryLocation, function (Handler, state) {
  React.render(<Handler />, document.body);
});