var keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({

    LOGIN_USER: null,
    LOGIN_USER_SUCCESS: null,
    LOGIN_USER_FAIL: null,

    LOGOUT_USER: null,
    LOGOUT_USER_SUCCESS: null,
    LOGOUT_USER_FAIL: null
});