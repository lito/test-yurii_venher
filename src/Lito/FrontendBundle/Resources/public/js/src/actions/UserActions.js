var UserConstants = require('../constants/UserConstants');

var AuthClient = require('../utils/AuthClient');

var UserActions = {
    loginUser: function(login, password) {
        var self = this;
        self.dispatch(UserConstants.LOGIN_USER);
        AuthClient.login(login, password, function(data) {
            self.dispatch(UserConstants.LOGIN_USER_SUCCESS, data);
        }, function() {
            self.dispatch(UserConstants.LOGIN_USER_FAIL);
        });
    },
    logoutUser: function() {
        var self = this;
        
        self.dispatch(UserConstants.LOGOUT_USER);
        AuthClient.logout(function(data) {
            self.dispatch(UserConstants.LOGOUT_USER_SUCCESS, data);
        }, function() {
            self.dispatch(UserConstants.LOGOUT_USER_FAIL);
        });
    }
};

module.exports = UserActions;