$ = require('jquery');

var AuthClient = {
  login: function(username, password, success, error) {
     var obj = {
        dataType: "json",
        contentType: "application/json", 
        type: 'POST',
        url: '/api/users/auths?login=' + username + '&password=' + password,
        success: function(response) {
          if (response.token) {
            localStorage.authToken = response.token;
            localStorage.userName = response.username;
            localStorage.isAdmin = response.isAdmin;
            localStorage.customerId = response.customerId;
            success(response);
            }
      },
        error: error
    };

    $.ajax(obj);

  },

  getToken: function() {
    return localStorage.authToken;
  },

  getUserName: function() {
    return localStorage.userName;
  },

  logout: function(success, error) {
    var obj = {
        dataType: "json",
        contentType: "application/json", 
        type: 'DELETE',
        url: '/api/user/auth?token=' + localStorage.authToken,
        success: function(response) {
          localStorage.clear();
          success(response);
      },
        error: error
    };

    $.ajax(obj);
    
  },

  loggedIn: function() {
    return !!localStorage.authToken;
  },

  isAdmin: function() {
    return !!localStorage.isAdmin;
  },


  customerId: function() {
    return !!localStorage.customerId;
  }

};

module.exports = AuthClient;