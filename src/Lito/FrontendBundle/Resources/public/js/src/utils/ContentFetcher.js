var Env = require("./Env");

// 
var $ = require('jquery');

function ContentFetcher(settings, success, error) {
    var obj = {
        dataType: "json",
        contentType: "application/json", 
        type: (settings.type || 'GET'),
        url: Env.getContentURL(settings.ident, settings.itemId),
        success: success,
        error: error

    };

    if(settings.upload) {
		obj.data = settings.data;
		obj.processData = false;
        obj.contentType = false;
    }
	
    if(settings.data && !settings.upload) {
    	obj.data = JSON.stringify(settings.data);

    }

    $.ajax(obj);
}

module.exports = ContentFetcher;