var BuzzwordClient = {
  load: function(success, failure) {
    setTimeout(function() {
      success(_.range(10).map(function(){return "tralalal";}));
    }, 1000);
  },

  submit: function(word, success, failure) {
    setTimeout(function() {
      if (Math.random() > 0.5) {
        success(word);
      } else {
        failure("Failed to fetch");
      }
    }, 1000);
  }
};

module.exports = BuzzwordClient;