<?php

/* LitoFrontendBundle:Default:index.html.twig */
class __TwigTemplate_52fd781f761350b86040650253b4264fb56afcddd92c3141b81e10714e3718db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html class=\"no-js\" lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta http-equiv=\"content-script-type\" content=\"text/javascript\">
    <title>Tatjana Prijmak ";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "environment", array()), "html", null, true);
        echo "</title>
    <meta name=\"description\" content=\"\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/litofrontend/js/build/css/bootstrap.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"//blueimp.github.io/Gallery/css/blueimp-gallery.min.css\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/litofrontend/js/dist/vendor/css/bootstrap-image-gallery.min.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css\" />
    
  </head>
  <body style=\"overflow-y: auto !important\">
    <!--[if lt IE 8]>
      <p class=\"browsehappy\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    ";
        // line 21
        if (($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "environment", array()) == "dev")) {
            // line 22
            echo "    <script type=\"text/javascript\">
      window._GLOBALS = {
        environment: 'dev'
      }
    </script>
    <script type=\"text/javascript\" src=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/litofrontend/js/build/app.js"), "html", null, true);
            echo "\"/></script>
    
    ";
        } else {
            // line 30
            echo "    <script type=\"text/javascript\">
      window._GLOBALS = {
        environment: 'test'
      }
    </script>
    <script type=\"text/javascript\" src=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/litofrontend/js/build/app.min.js"), "html", null, true);
            echo "\"/></script>
    ";
        }
        // line 37
        echo "  
  </body>
</html>

";
    }

    public function getTemplateName()
    {
        return "LitoFrontendBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 37,  72 => 35,  65 => 30,  59 => 27,  52 => 22,  50 => 21,  38 => 12,  33 => 10,  27 => 7,  19 => 1,);
    }
}
